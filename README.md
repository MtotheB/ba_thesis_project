# BA_Thesis_Project

## Introduction

This project was part of my Bachelor Thesis. The goal was to develop and implement a VR environment, in which the expertise of a user (for a given task) can be ascertained. It therefor also serves as base project for further developments, in which users would be interactively guided in their learning process.

## Game

Within the VR environment users have to solve certain tasks in a game, which is much like the game Robo Rally.
Several things are measured and logged for later data analysis.

## Installation

If you want to import the project directly into Unity you can do so by using Unity Editor 2020.3.12f1.

You also can right away test it by starting the executable from the build folder.