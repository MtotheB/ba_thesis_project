using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideHand : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HideIt()
    {
        //Debug.LogError("Hide ME!");
        Transform parent = transform.parent;
        if (parent != null)
            parent.gameObject.SetActive(false);        
        else
            Debug.LogError("[ERROR]: HideHand Script parent transform is empty!");
    }

    public void ShowIt()
    {
        //Debug.LogError("Show ME!");
        Transform parent = transform.parent;
        if (parent != null)
            parent.gameObject.SetActive(true);        
        else
            Debug.LogError("[ERROR]: HideHand Script parent transform is empty!");
    }
}
