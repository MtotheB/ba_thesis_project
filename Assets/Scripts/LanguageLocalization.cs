using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization.Settings;

public class LanguageLocalization : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Toggles the language for localization
    /// between English and German.
    /// </summary>
    public void ToggleLanguage()
    {
        Debug.Log("Changing Language. Actual LocalName: >" + LocalizationSettings.SelectedLocale.LocaleName + "<");

        if (LocalizationSettings.SelectedLocale.name == "German (de)")
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[0];
        else
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[1];
    }
}
