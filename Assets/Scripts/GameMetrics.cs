using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMetrics : MonoBehaviour
{
    /// <summary>
    /// Stores the headset positions
    /// </summary>
    [HideInInspector]
    public List<Vector3> HeadSetPositions;

    /// <summary>
    /// Stores the local positions of the headset.
    /// </summary>
    [HideInInspector]
    public List<Vector3> HeadSetLocalPositions;

    /// <summary>
    /// Stores the rotation of the headset.
    /// </summary>
    [HideInInspector]
    public List<Quaternion> HeadSetRotations;

    /// <summary>
    /// Stores the local rotation of the headset.
    /// </summary>
    [HideInInspector]
    public List<Quaternion> HeadSetLocalRotations;

    /// <summary>
    /// Stores the positions of the left hand.
    /// </summary>
    [HideInInspector]
    public List<Vector3> LeftHandPositions;

    /// <summary>
    /// Stores the positions of the right hand.
    /// </summary>
    [HideInInspector]
    public List<Vector3> RightHandPositions;

    /// <summary>
    /// Stores the local positions of the left hand.
    /// </summary>
    [HideInInspector]
    public List<Vector3> LeftHandLocalPositions;

    /// <summary>
    /// Stores the local positions of the right hand.
    /// </summary>
    [HideInInspector]
    public List<Vector3> RightHandLocalPositions;

    /// <summary>
    /// Stores the rotation of the left hand.
    /// </summary>
    [HideInInspector]
    public List<Quaternion> LeftHandRotations;

    /// <summary>
    /// Stores the rotation of the right hand.
    /// </summary>
    [HideInInspector]
    public List<Quaternion> RightHandRotations;

    /// <summary>
    /// Stores the rotation of the left hand.
    /// </summary>
    [HideInInspector]
    public List<Quaternion> LeftHandLocalRotations;

    /// <summary>
    /// Stores the rotation of the right hand.
    /// </summary>
    [HideInInspector]
    public List<Quaternion> RightHandLocalRotations;

    /// <summary>
    /// Stores controller actions.
    /// </summary>
    public List<string> ControllerActions;

    /// <summary>
    /// Stores the times when a controller action took place.
    /// </summary>
    public List<float> ControllerActionTimes;

    // Start is called before the first frame update
    void Start()
    {
        HeadSetPositions = new List<Vector3>();
        HeadSetLocalPositions = new List<Vector3>();
        HeadSetRotations = new List<Quaternion>();
        HeadSetLocalRotations = new List<Quaternion>();
        RightHandPositions = new List<Vector3>();
        RightHandLocalPositions = new List<Vector3>();
        LeftHandPositions = new List<Vector3>();
        LeftHandLocalPositions = new List<Vector3>();
        RightHandRotations = new List<Quaternion>();
        RightHandLocalRotations = new List<Quaternion>();
        LeftHandRotations = new List<Quaternion>();
        LeftHandLocalRotations = new List<Quaternion>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
