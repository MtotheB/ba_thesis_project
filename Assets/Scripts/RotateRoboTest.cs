using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateRoboTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        GameController gameController = GameObject.Find("GameController").GetComponent<GameController>();

        bool rotateRight = gameObject.name == "RotateRight";

        if (gameController != null)
            gameController.TestRotation(rotateRight);
        else
            Debug.LogError("[ERROR] Couldn't find GameController.");

    }
}
