using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBtnTestScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        GameController gameController = GameObject.Find("GameController").GetComponent<GameController>();

        if (gameController != null)
            gameController.ResetField();
        else
            Debug.LogError("[ERROR] Couldn't find GameController.");
    }

}
