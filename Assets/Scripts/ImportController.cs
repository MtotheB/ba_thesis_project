using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ImportController : MonoBehaviour
{
    [SerializeField]
    private GameController gameController;

    [SerializeField]
    private LevelBuilder levelBuilder;

    [SerializeField]
    private TextAsset levelCSV;

    [SerializeField]
    private TextAsset metricsCSV;

    [SerializeField]
    [Tooltip("The separator used in the CSV file(s).")]
    private char fieldSeparator = ';';

    /// <summary>
    /// The line separator.
    /// </summary>
    private char lineSeparator = '\n';

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //-- LEVEL LOADING --//

    /// <summary>
    /// Returns the byte matrix, that
    /// represents a game field matrix
    /// for a certain saved level.
    /// </summary>
    /// <param name="number">The level number of the game field matrix.</param>
    /// <returns>The game field matrix for a certain saved level.</returns>
    internal byte[,] GetFieldMatrix(int number)
    {        
        //read raw data first
        string[] records = ReadCSVData();
        //get intervall for matrix length
        (int, int) intervall = GetRecordIntervall(records, number, false);
        //get matrix
        byte[,] matrix = GetMatrixInIntervall(intervall, records);

        //fix standard fields partially        
        //only needed if level matrices
        //are not saved correctly for standard fields
        //matrix = FixStandardFields(matrix, records, number);
        return matrix;
    }

    /// <summary>
    /// Workaround to set all walked fields from
    /// 0 to the correct byte representation within the 
    /// matrix to have a correct highlighting of the 
    /// standard fields, which the robo has walked 
    /// when loading (recreating) a certain level.
    /// 
    /// ONLY WORKS WHEN CARDS ALSO HAVE BEEN PLACED CORRECTLY
    /// SINCE THE CORRECT COMBINATIONS ARE NOT SAVED RIGHT!
    /// </summary>
    /// <param name="matrix">The matrix to fix.</param>
    /// <param name="records">The records from the level.csv.</param>
    /// <param name="number">The level number to load.</param>
    /// <returns>A fixed matrix where the standard fields, that have been walked by the robo are set to the correct byte representation.</returns>
    private byte[,] FixStandardFields(byte[,] matrix, string[] records, int number)
    {
        if (levelBuilder == null)
            return matrix;

        //WONT WORK BECAUSE CORRECT COMBINATIONS ARE NOT SAVED CORRECTLY!
        //first get intervall for this level number
        //(int, int) intervall = GetCorrectCombinationsIntervall(records, number);
        //Debug.Log("INTERVALL FOR CORRECT COMBINATIONS -> startIndex: " + intervall.Item1 + ", endIndex: " + intervall.Item2);


        //check if all cards where placed correctly
        int numberCorrectCards = GetNumberOfCorrectPlacedCards(records, number);
        //leave if not all cards had been placed correctly
        if (numberCorrectCards != levelBuilder.NumberProgrammingCards)
        {
            Debug.Log("FOR LEVEL NR. " + number + " THE CARDS HAVE NOT BEEN PLACED IN CORRECT ORDER!");
            return matrix;
        }

        //get intervall for placed cards
        (int, int) intervall = GetPlacedCardsIntervall(records, number);

        Debug.Log("INTERVALL FOR PLACED CARDS -> startIndex: " + intervall.Item1 + ", endIndex: " + intervall.Item2);

        byte orientation = 0;        

        //get coordination of start field
        (int, int) actualCoords = levelBuilder.GetCoordsOfField(matrix, levelBuilder.StartFieldRepresentation);

        //go through combinations at correct index
        for (int i = intervall.Item1; i < intervall.Item2 + 1; i++)
        {
            string[] fields = records[i].Split(fieldSeparator);
                        
            if (!PartCardReferences.TryGetValue(fields[1], out string partCard))
                return matrix;
            
            //orientation change from card
            orientation = GetChangedOrientation(partCard, orientation, true);
            //number of moves from card
            int numberMoves = levelBuilder.GetNumberOfMovesCard(partCard);
                        
            //process moves from card in correct direction
            for (int j = 0; j < numberMoves; j++)
            {               
                switch (orientation)
                {
                    case 0:
                        actualCoords.Item2 += 1;
                        break;
                    case 1:
                        actualCoords.Item1 += 1;
                        break;
                    case 2:
                        actualCoords.Item2 -= 1;
                        break;
                    default:
                        actualCoords.Item1 -= 1;
                        break;
                }

                //now change the walked field to correct standard field
                if (matrix[actualCoords.Item1, actualCoords.Item2] == 0)
                    matrix[actualCoords.Item1, actualCoords.Item2] = levelBuilder.StandardFieldWalkedRepresentation;                
            }

            //now process field
            if (!levelBuilder.PredefinedGameFields.TryGetValue(matrix[actualCoords.Item1, actualCoords.Item2], out string partField))
                return matrix;

            orientation = GetChangedOrientation(partField, orientation, false);
            numberMoves = levelBuilder.GetNumberOfMovesField(partField);
                     
            byte[,] orientations = GetOrientations(number);

            //process moves from field in correct direction
            for (int j = 0; j < numberMoves; j++)
            {
                switch (orientations[actualCoords.Item1, actualCoords.Item2])
                {
                    case 0:
                        actualCoords.Item2 += 1;
                        break;
                    case 1:
                        actualCoords.Item1 += 1;
                        break;
                    case 2:
                        actualCoords.Item2 -= 1;
                        break;
                    default:
                        actualCoords.Item1 -= 1;
                        break;
                }

                //now change the walked field to correct standard field
                if (matrix[actualCoords.Item1, actualCoords.Item2] == 0)
                    matrix[actualCoords.Item1, actualCoords.Item2] = levelBuilder.StandardFieldWalkedRepresentation;                
            }
        }

        return matrix;
    }
        
    /// <summary>
    /// Returns the changed orientation for the robo
    /// after the card or field move has been processed.
    /// </summary>
    /// <param name="part">The card or field to check for.</param>
    /// <param name="actualOrientation">The actual orientation of the robo.</param>
    /// <param name="card">Whether the card move shall be checked or not.</param>
    /// <returns>The orientation change for the robo after this field or card has been processed.</returns>
    private byte GetChangedOrientation(string part, byte actualOrientation, bool card)
    {
        byte newOrientation = actualOrientation;

        if(card)
        {
            switch (part)
            {
                case "C_RR":
                    newOrientation = GetNewOrientation(1, actualOrientation);
                    break;
                case "C_RL":
                    newOrientation = GetNewOrientation(-1, actualOrientation);
                    break;
                case "C_UT":
                    newOrientation = GetNewOrientation(2, actualOrientation);
                    break;
                default:
                    break;
            }
        }
        else
        {
            switch (part)
            {
                case "F_RR":
                    newOrientation = GetNewOrientation(1, actualOrientation);                    
                    break;
                case "F_RL":
                    newOrientation = GetNewOrientation(-1, actualOrientation);                    
                    break;
                default:
                    break;
            }
        }
        
        return newOrientation;
    }

    /// <summary>
    /// Holds references between shortcuts for
    /// programming cards and their ingame 
    /// name.
    /// </summary>
    private Dictionary<string, string> PartCardReferences = new Dictionary<string, string>
    {
        {" Move1(Clone)", "C_M1"},
        {" Move2(Clone)", "C_M2"},
        {" Move3(Clone)", "C_M3"},
        {" Back(Clone)", "C_B"},
        {" RRight(Clone)", "C_RR"},
        {" RLeft(Clone)", "C_RL"},
        {" UTurn(Clone)", "C_UT"},
    };

    /// <summary>
    /// Returns the new orientation for a certain change.
    /// </summary>
    /// <param name="orientationChange">The rotation change for the robo.</param>
    /// <param name="actualOrientation">The actual orientation.</param>
    /// <returns>The new orientation after a certain change.</returns>
    private byte GetNewOrientation(int orientationChange, byte actualOrientation)
    {
        int newOrienation = 0;
        if (orientationChange > 0 || orientationChange < 0)
        {
            int sum = actualOrientation + orientationChange;
            if (sum > levelBuilder.NUMBER_ORIENTATIONS - 1)
                newOrienation = sum - levelBuilder.NUMBER_ORIENTATIONS;
            else if (sum < 0)
                newOrienation = levelBuilder.NUMBER_ORIENTATIONS - 1;
            else
                newOrienation = sum;
        }
        return (byte)newOrienation;
    }

    /// <summary>
    /// Returns the number of correct placed
    /// cards from the saved level.csv for a certain level.
    /// </summary>
    /// <param name="records">The saved records from level.csv.</param>
    /// <param name="number">The level number for which the number should be searched for.</param>
    /// <returns>The number of correct placed cards for a certain level.</returns>
    private int GetNumberOfCorrectPlacedCards(string[] records, int number)
    {
        int correct = 0;
        int counter = 0;
        bool started = false;
                
        foreach (string record in records)
        {
            string[] fields = record.Split(fieldSeparator);

            if (fields.Length > 1 && fields[0] != "NUMBER OF CORRECT PLACED CARDS" && !started)
            {
                counter += 1;
                continue;
            }
            else if (fields[0] == "NUMBER OF CORRECT PLACED CARDS")
            {
                started = true;
            }

            //check for correct level number
            if (fields.Length == 2 && !(fields[0] == "LEVEL") && !(fields[0] == "NUMBER OF CORRECT PLACED CARDS"))
            {
                int parsed = -1;
                try
                {
                    parsed = Int32.Parse(fields[0]);
                }
                catch (Exception)
                {
                    Debug.LogError($"Unable to parse '{fields[0]}'");
                }

                //level found
                if (parsed == number)
                {
                    try
                    {
                        correct = Int32.Parse(fields[1]);
                    }
                    catch (Exception)
                    {
                        Debug.LogError($"Unable to parse '{fields[1]}'");
                    }

                    break;                    
                }
            }           

            counter += 1;
        }

        return correct;
    }

    /// <summary>
    /// Returns the intervall for the entries of the 
    /// placed cards in level.csv.
    /// </summary>
    /// <param name="records">The records from level.csv.</param>
    /// <param name="number">The level number to search the index for.</param>
    /// <returns>The intervall from the entries for placed cards for a certain level.</returns>
    private (int, int) GetPlacedCardsIntervall(string[] records, int number)
    {
        (int, int) intervall = (0, 0);

        int startIndex = 0;
        int endIndex = 0;
        int counter = 0;
        bool started = false;
        bool levelFound = false;
        
        foreach (string record in records)
        {
            string[] fields = record.Split(fieldSeparator);

            if (fields.Length > 1 && fields[0] != "PLACED CARDS" && !started)
            {
                counter += 1;
                continue;
            }
            else if (fields[0] == "PLACED CARDS")
            {
                started = true;
            }

            //check for correct level number
            if (fields.Length == 2 && !(fields[0] == "LEVEL") && !(fields[0] == "PLACED CARDS"))
            {
                int parsed = -1;
                try
                {
                    parsed = Int32.Parse(fields[0]);
                }
                catch (Exception)
                {
                    Debug.LogError($"Unable to parse '{fields[0]}'");
                }

                //level found
                if (parsed == number)
                {
                    if (!levelFound)
                    {
                        startIndex = counter;
                        levelFound = true;
                    }
                }
            }

            if (levelFound)
            {
                int parsed = -1;
                try
                {
                    parsed = Int32.Parse(fields[0]);
                }
                catch (Exception)
                {
                    Debug.LogError($"Unable to parse '{fields[0]}'");
                }

                if (parsed != number)
                {
                    endIndex = counter - 1;
                    break;
                }
            }

            counter += 1;
        }

        intervall.Item1 = startIndex;
        intervall.Item2 = endIndex;

        return intervall;
    }

    /// <summary>
    /// Returns the intervall of the correct combinations
    /// from the saved level.csv.
    /// </summary>
    /// <param name="records">The saved records from level.csv.</param>
    /// <param name="number">The level number for which the intervall should be searched for.</param>
    /// <returns>The intervall of the saved correct combinations for a certain saved level.</returns>
    private (int, int) GetCorrectCombinationsIntervall(string[] records, int number)
    {
        (int, int) intervall = (0, 0);

        int startIndex = 0;
        int endIndex = 0;
        int counter = 0;
        bool started = false;
        bool levelFound = false;
        //first get correct card-/field-combinations
        foreach (string record in records)
        {
            string[] fields = record.Split(fieldSeparator);
          
            if (fields.Length > 1 && fields[0] != "CORRECT COMBINATIONS" && !started)
            {
                counter += 1;
                continue;
            }
            else if (fields[0] == "CORRECT COMBINATIONS")
            {
                started = true;
            }

            //check for correct level number
            if (fields.Length == 2 && !(fields[0] == "LEVEL") && !(fields[0] == "CORRECT COMBINATIONS"))
            {
                int parsed = -1;
                try
                {
                    parsed = Int32.Parse(fields[0]);
                }
                catch (Exception)
                {
                    Debug.LogError($"Unable to parse '{fields[0]}'");
                }

                //level found
                if (parsed == number)
                {
                    if (!levelFound)
                    {
                        startIndex = counter;
                        levelFound = true;
                    }
                }
            }

            if(levelFound)
            {
                int parsed = -1;
                try
                {
                    parsed = Int32.Parse(fields[0]);
                }
                catch (Exception)
                {
                    Debug.LogError($"Unable to parse '{fields[0]}'");
                }

                if(parsed != number)
                {
                    endIndex = counter - 1;
                    break;
                }
            }

            counter += 1;
        }

        intervall.Item1 = startIndex;
        intervall.Item2 = endIndex;

        return intervall;
    }

    /// <summary>
    /// Returns the byte matrix, that
    /// represents the game field orientations
    /// for a certain saved level.
    /// </summary>
    /// <param name="number">The level number of the game field matrix.</param>
    /// <returns>The game field orientations for a certain saved level.</returns>
    internal byte[,] GetOrientations(int number)
    {
        //read raw data first
        string[] records = ReadCSVData();
        //get intervall for matrix length
        (int, int) intervall = GetRecordIntervall(records, number, true);
        //get matrix
        byte[,] matrix = GetMatrixInIntervall(intervall, records);
        
        return matrix;
    }

    /// <summary>
    /// Returns the cards for a certain saved level.
    /// </summary>
    /// <param name="number">The level number to get the cards for.</param>
    /// <returns>The cards for a certain saved level.</returns>
    internal string[] GetCards(int number)
    {

        return new string[0];
    }

    /// <summary>
    /// Reads in the CSV Data for a level.
    /// </summary>
    /// <returns>The records from level.csv.</returns>
    private string[] ReadCSVData()
    {
        string[] records = levelCSV.text.Split(lineSeparator);
        return records;
    }

    /// <summary>
    /// Returns the intervall for a byte matrix
    /// for the game fields or their orientations
    /// for a certain saved level.
    /// </summary>
    /// <param name="records">The raw data, read from level.csv.</param>
    /// <param name="number">The number of the level to search for.</param>
    /// <param name="orientations">Whether the intervall for the orientations matrix should be returned or not.</param>
    /// <returns>The intervall for the game field matrix or the orientation matrix within the raw data.</returns>
    private (int, int) GetRecordIntervall(string[] records, int number, bool orientations)
    {
        (int, int) intervall = (0, 0);

        //go through records
        int counter = 0;
        int startIndex = 0;
        int endIndex = 0;
        bool levelFound = false;
        bool orientationsStarted = false;
        foreach (string record in records)
        {
            string[] fields = record.Split(fieldSeparator);

            string fieldOrOrientationMatrix = orientations ? "ORIENTATIONS" : "FIELD MATRICES";

            if (orientations && fields[0] != "ORIENTATIONS" && !orientationsStarted)
            {
                counter += 1;
                continue;
            }
            else if(orientations && fields[0] == "ORIENTATIONS")
            {
                orientationsStarted = true;
            }

            //check for correct level number
            if (fields.Length == 1 && !(fields[0] == "LEVEL") && !(fields[0] == fieldOrOrientationMatrix))
            {
                int parsed = -1;
                try
                {
                    parsed = Int32.Parse(fields[0]);                    
                }
                catch (Exception)
                {
                    Debug.LogError($"Unable to parse '{fields[0]}'");
                }

                //level found
                if (parsed == number)
                {
                    startIndex = counter + 1;
                    levelFound = true;
                }
            }

            //get last index of matrix
            if (fields.Length > 0 && fields[0] == "LEVEL" && counter > 2 && levelFound)
            {
                endIndex = counter - 1;
                break;
            }

            counter += 1;
        }

        intervall.Item1 = startIndex;
        intervall.Item2 = endIndex;
        return intervall;
    }

    /// <summary>
    /// Returns the matrix within a given
    /// intervall from the raw data.
    /// </summary>
    /// <param name="intervall">The start and end index to search for.</param>
    /// <param name="records">The raw data from level.csv.</param>
    /// <returns>The matrix within a given intervall.</returns>
    private byte[,] GetMatrixInIntervall((int, int) intervall, string[] records)
    {
        byte[,] matrix = new byte[(intervall.Item2 - intervall.Item1) + 1, (intervall.Item2 - intervall.Item1) + 1];

        //now read in only those records within
        //the found intervall
        int counter = 0;
        for (int i = intervall.Item1; i < intervall.Item2; i++)
        {
            int counterField = 0;
            string[] fields = records[i].Split(fieldSeparator);
            foreach (var field in fields)
            {
                byte parsed = 0;
                try
                {
                    parsed = Byte.Parse(field);
                    matrix[counter, counterField] = parsed;
                    counterField += 1;
                }
                catch (Exception)
                {
                    //Debug.LogError($"Unable to parse '{field}'");
                }
            }
            counter += 1;
        }

        return matrix;
    }

}
