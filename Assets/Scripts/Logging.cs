using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using UnityEditor;

public class Logging : MonoBehaviour
{

    [SerializeField]
    [Tooltip("The controller for the game timer.")]
    private GameTimer timerController;

    [SerializeField]
    [Tooltip("To store game metrics.")]
    private GameMetrics gameMetrics;

    [SerializeField]
    [Tooltip("To store level data.")]
    private LevelData levelData;

    [SerializeField]
    [Tooltip("The intervall in seconds in which game metrics should be saved.")]
    private float logIntervallMetrics = 1.0f;

    [SerializeField]
    [Tooltip("The main camera of the XR Rig. To be used to log HMD position")]
    private GameObject mainCamera;

    [SerializeField]
    [Tooltip("The left hand controller")]
    private GameObject leftHandController;

    [SerializeField]
    [Tooltip("The right hand controller")]
    private GameObject rightHandController;

    /// <summary>
    /// The file path for the game metrics data.
    /// </summary>
    private string fileMetrics = "";

    /// <summary>
    /// The file path for the survey data.
    /// </summary>
    private string fileSurvey = "";

    /// <summary>
    /// The file path for the level/game data.
    /// </summary>
    private string fileLevelData = "";

    /// <summary>
    /// True if coroutine is still running.
    /// </summary>
    private bool isLogging;

    // Start is called before the first frame update
    void Start()
    {
        fileMetrics = Application.persistentDataPath + "/metrics.csv";
        fileSurvey = Application.persistentDataPath + "/survey.csv";
        fileLevelData = Application.persistentDataPath + "/level.csv";
    }

    // Update is called once per frame
    void Update()
    {
        if(!isLogging)
            StartCoroutine(DelayedLog());
    }

    IEnumerator DelayedLog()
    {
        isLogging = true;
        LogHMDPosition();
        LogControllerPosition();        
        yield return new WaitForSeconds(logIntervallMetrics);
        isLogging = false;
    }

    private void LogControllerPosition()
    {
        if(leftHandController == null || rightHandController == null)
        {
            Debug.Log("One or both controllers not assigned!");
            return;
        }

        if(gameMetrics == null)
        {
            Debug.Log("Game metrics not assigned!");
            return;
        }

        gameMetrics.LeftHandPositions.Add(leftHandController.transform.position);
        gameMetrics.LeftHandLocalPositions.Add(leftHandController.transform.localPosition);
        gameMetrics.LeftHandRotations.Add(leftHandController.transform.rotation);
        gameMetrics.LeftHandLocalRotations.Add(leftHandController.transform.localRotation);
        gameMetrics.RightHandPositions.Add(rightHandController.transform.position);
        gameMetrics.RightHandLocalPositions.Add(rightHandController.transform.localPosition);
        gameMetrics.RightHandRotations.Add(rightHandController.transform.rotation);
        gameMetrics.RightHandLocalRotations.Add(rightHandController.transform.localRotation);
    }

    /// <summary>
    /// Saves the position of the headset at 
    /// each intervall. The intervall is given
    /// with <see cref="logIntervallMetrics"/>.
    /// As Position for the headset the 
    /// main camera of the XR Rig will be used.
    /// </summary>
    private void LogHMDPosition()
    {
        if (mainCamera == null)
        {
            Debug.Log("Main Camera not assigned!");
            return;
        }
        
        if(gameMetrics == null)
        {
            Debug.Log("Game metrics not assigned.");
            return;
        }

        gameMetrics.HeadSetPositions.Add(mainCamera.transform.position);
        gameMetrics.HeadSetLocalPositions.Add(mainCamera.transform.localPosition);
        gameMetrics.HeadSetRotations.Add(mainCamera.transform.rotation);
        gameMetrics.HeadSetLocalRotations.Add(mainCamera.transform.localRotation);
    }
       
    /// <summary>
    /// Writes all lists from saved data
    /// in <see cref="GameMetrics"/> to
    /// a CSV file.
    /// </summary>
    public void WriteGameMetricsToCSV()
    {
        if (gameMetrics == null)
            return;

        //delete file first if it had been 
        //created before
        //FileUtil.DeleteFileOrDirectory(fileMetrics);
        //doesn't work on Quest

        int rand = Random.Range(1, 50000);

        if (File.Exists(fileMetrics))
            fileMetrics = Application.persistentDataPath + "/metrics" + rand + ".csv";

        //headset
        File.AppendAllText(fileMetrics, "HEADSET POSITIONS\n");
        File.AppendAllText(fileMetrics, "LOGNR.; POSITION\n");

        int counter = 1;

        foreach (var data in gameMetrics.HeadSetPositions)
        {
            File.AppendAllText(fileMetrics, $"{counter}; {data}\n");
            counter += 1;
        }

        File.AppendAllText(fileMetrics, "HEADSET LOCAL POSITIONS\n");
        File.AppendAllText(fileMetrics, "LOGNR.; LOCAL POSITION\n");

        counter = 1;

        foreach (var data in gameMetrics.HeadSetLocalPositions)
        {
            File.AppendAllText(fileMetrics, $"{counter}; {data}\n");
            counter += 1;
        }

        File.AppendAllText(fileMetrics, "HEADSET ROTATIONS\n");
        File.AppendAllText(fileMetrics, "LOGNR.; ROTATION\n");

        counter = 1;

        foreach (var data in gameMetrics.HeadSetRotations)
        {
            File.AppendAllText(fileMetrics, $"{counter}; {data}\n");
            counter += 1;
        }

        File.AppendAllText(fileMetrics, "HEADSET LOCAL ROTATIONS\n");
        File.AppendAllText(fileMetrics, "LOGNR.; LOCAL ROTATION\n");

        counter = 1;

        foreach (var data in gameMetrics.HeadSetLocalRotations)
        {
            File.AppendAllText(fileMetrics, $"{counter}; {data}\n");
            counter += 1;
        }

        //controllers
        File.AppendAllText(fileMetrics, "LEFT CONTROLLER POSITIONS\n");
        File.AppendAllText(fileMetrics, "LOGNR.; POSITION\n");

        counter = 1;

        foreach (var data in gameMetrics.LeftHandPositions)
        {
            File.AppendAllText(fileMetrics, $"{counter}; {data}\n");
            counter += 1;
        }

        File.AppendAllText(fileMetrics, "LEFT CONTROLLER LOCAL POSITIONS\n");
        File.AppendAllText(fileMetrics, "LOGNR.; LOCAL POSITION\n");

        counter = 1;

        foreach (var data in gameMetrics.LeftHandLocalPositions)
        {
            File.AppendAllText(fileMetrics, $"{counter}; {data}\n");
            counter += 1;
        }

        File.AppendAllText(fileMetrics, "RIGHT CONTROLLER POSITIONS\n");
        File.AppendAllText(fileMetrics, "LOGNR.; POSITION\n");

        counter = 1;

        foreach (var data in gameMetrics.RightHandPositions)
        {
            File.AppendAllText(fileMetrics, $"{counter}; {data}\n");
            counter += 1;
        }

        File.AppendAllText(fileMetrics, "RIGHT CONTROLLER LOCAL POSITIONS\n");
        File.AppendAllText(fileMetrics, "LOGNR.; LOCAL POSITION\n");

        counter = 1;

        foreach (var data in gameMetrics.RightHandLocalPositions)
        {
            File.AppendAllText(fileMetrics, $"{counter}; {data}\n");
            counter += 1;
        }

        File.AppendAllText(fileMetrics, "LEFT CONTROLLER ROTATION\n");
        File.AppendAllText(fileMetrics, "LOGNR.; ROTATION\n");

        counter = 1;

        foreach (var data in gameMetrics.LeftHandRotations)
        {
            File.AppendAllText(fileMetrics, $"{counter}; {data}\n");
            counter += 1;
        }

        File.AppendAllText(fileMetrics, "LEFT CONTROLLER LOCAL ROTATION\n");
        File.AppendAllText(fileMetrics, "LOGNR.; LOCAL ROTATION\n");

        counter = 1;

        foreach (var data in gameMetrics.LeftHandLocalRotations)
        {
            File.AppendAllText(fileMetrics, $"{counter}; {data}\n");
            counter += 1;
        }

        File.AppendAllText(fileMetrics, "RIGHT CONTROLLER ROTATION\n");
        File.AppendAllText(fileMetrics, "LOGNR.; ROTATION\n");

        counter = 1;

        foreach (var data in gameMetrics.RightHandRotations)
        {
            File.AppendAllText(fileMetrics, $"{counter}; {data}\n");
            counter += 1;
        }

        File.AppendAllText(fileMetrics, "RIGHT CONTROLLER LOCAL ROTATION\n");
        File.AppendAllText(fileMetrics, "LOGNR.; LOCAL ROTATION\n");

        counter = 1;

        foreach (var data in gameMetrics.RightHandLocalRotations)
        {
            File.AppendAllText(fileMetrics, $"{counter}; {data}\n");
            counter += 1;
        }

        File.AppendAllText(fileMetrics, "CONTROLLER ACTIONS\n");
        File.AppendAllText(fileMetrics, "TIME; ACTION\n");
        
        for (int i = 0; i < gameMetrics.ControllerActions.Count; i++)
        {
            File.AppendAllText(fileMetrics, $"{gameMetrics.ControllerActionTimes[i]}; {gameMetrics.ControllerActions[i]}\n");
        }
    }

    /// <summary>
    /// Writes the lists of the survey data
    /// (from <see cref="SurveyData"/>> to 
    /// an CSV file.
    /// </summary>
    public void WriteSurveyDataToCSV(SurveyData surveyData)
    {
        if (surveyData == null)
            return;

        //FileUtil.DeleteFileOrDirectory(fileSurvey);
        //File.Delete(fileSurvey);
        //File.Create(fileSurvey);

        int rand = Random.Range(1, 50000);

        if (File.Exists(fileSurvey))
            fileSurvey = Application.persistentDataPath + "/survey" + rand + ".csv";

        //mental effort
        File.AppendAllText(fileSurvey, "MENTAL EFFORT\n");
        File.AppendAllText(fileSurvey, "LEVEL; VALUE\n");

        int levelCounter = 1;

        foreach (var data in surveyData.MentalEffort)
        {
            File.AppendAllText(fileSurvey, $"{levelCounter}; {data}\n");
            levelCounter += 1;
        }

        //complexity 
        File.AppendAllText(fileSurvey, "COMPLEXITY\n");
        File.AppendAllText(fileSurvey, "LEVEL; VALUE\n");

        levelCounter = 1;

        foreach (var data in surveyData.Complexity)
        {
            File.AppendAllText(fileSurvey, $"{levelCounter}; {data}\n");
            levelCounter += 1;
        }

        File.AppendAllText(fileSurvey, "\n");
        File.AppendAllText(fileSurvey, "COMBINATION DIFFICULT\n");
        File.AppendAllText(fileSurvey, "LEVEL; CARD; FIELD; VALUE\n");

        levelCounter = 1;

        //combination difficulties
        for (int i = 0; i < surveyData.CombinationDifficulty.Count; i++)
        {
            for (int j = 0; j < surveyData.CombinationDifficulty[i].Count; j++)
            {
                string card = surveyData.CombinationCards[i][j];
                string field = surveyData.CombinationFields[i][j];
                int value = surveyData.CombinationDifficulty[i][j];
                File.AppendAllText(fileSurvey, $"{levelCounter}; {card}; {field}; {value}\n");
            }
            levelCounter += 1;
        }

        //final card difficulties
        File.AppendAllText(fileSurvey, "FINAL CARD DIFFICULTIES\n");
        File.AppendAllText(fileSurvey, "LEVEL; VALUE\n");

        levelCounter = 1;

        foreach (var data in surveyData.CardDifficulties)
        {
            File.AppendAllText(fileSurvey, $"{levelCounter}; {data}\n");
            levelCounter += 1;
        }

        //final field difficulties
        File.AppendAllText(fileSurvey, "FINAL FIELD DIFFICULTIES\n");
        File.AppendAllText(fileSurvey, "LEVEL; VALUE\n");

        levelCounter = 1;

        foreach (var data in surveyData.FieldDifficulties)
        {
            File.AppendAllText(fileSurvey, $"{levelCounter}; {data}\n");
            levelCounter += 1;
        }
    }

    /// <summary>
    /// Writes the lists of the <see cref="LevelData"/>
    /// to a CSV file.
    /// </summary>
    public void WriteLevelDataToCSV()
    {
        if (levelData == null)
            return;

        //FileUtil.DeleteFileOrDirectory(fileLevelData);
        //File.Delete(fileLevelData);
        //File.Create(fileLevelData);

        int rand = Random.Range(1, 50000);

        if (File.Exists(fileLevelData))
            fileLevelData = Application.persistentDataPath + "/level" + rand + ".csv";

        //field matrices
        File.AppendAllText(fileLevelData, "FIELD MATRICES\n");

        //level number
        for (int i = 0; i < levelData.SavedLevelMatrices.GetLength(0); i++)
        {
            File.AppendAllText(fileLevelData, "LEVEL\n");
            File.AppendAllText(fileLevelData, $"{i}\n");
                    
            //row
            for (int j = 0; j < levelData.SavedLevelMatrices.GetLength(1); j++)
            {
                //col
                for (int k = 0; k < levelData.SavedLevelMatrices.GetLength(2); k++)
                {
                    File.AppendAllText(fileLevelData, $"{levelData.SavedLevelMatrices[i,j,k]};");
                }
                File.AppendAllText(fileLevelData, $"\n");
            }
        }

        //field matrices
        File.AppendAllText(fileLevelData, "ORIENTATIONS\n");

        //level number
        for (int i = 0; i < levelData.SavedFieldOrientations.GetLength(0); i++)
        {
            File.AppendAllText(fileLevelData, "LEVEL\n");
            File.AppendAllText(fileLevelData, $"{i}\n");

            //row
            for (int j = 0; j < levelData.SavedFieldOrientations.GetLength(1); j++)
            {
                //col
                for (int k = 0; k < levelData.SavedFieldOrientations.GetLength(2); k++)
                {
                    File.AppendAllText(fileLevelData, $"{levelData.SavedFieldOrientations[i, j, k]};");
                }
                File.AppendAllText(fileLevelData, $"\n");
            }
        }

        //cards
        File.AppendAllText(fileLevelData, "PLACED CARDS\n");
        File.AppendAllText(fileLevelData, "LEVEL; CARD\n");

        for (int i = 0; i < levelData.PlacedCards.Count; i++)
        {            
            for (int j = 0; j < levelData.PlacedCards[i].Count; j++)
            {                
                File.AppendAllText(fileLevelData, $"{i}; {levelData.PlacedCards[i][j]}\n");
            }            
        }

        //correct combinations
        File.AppendAllText(fileLevelData, "CORRECT COMBINATIONS\n");
        File.AppendAllText(fileLevelData, "LEVEL; COMBINATION\n");

        for (int i = 0; i < levelData.CorrectCombinations.Count; i++)
        {
            for (int j = 0; j < levelData.CorrectCombinations[i].Count; j++)
            {
                File.AppendAllText(fileLevelData, $"{i}; {levelData.CorrectCombinations[i][j]}\n");
            }
        }

        //correct placed cards
        File.AppendAllText(fileLevelData, "NUMBER OF CORRECT PLACED CARDS\n");
        File.AppendAllText(fileLevelData, "LEVEL; CORRECT\n");

        for (int i = 0; i < levelData.CorrectPlacedCards.Count; i++)
        {
           File.AppendAllText(fileLevelData, $"{i}; {levelData.CorrectPlacedCards[i]}\n");
        }

        //times between cards
        File.AppendAllText(fileLevelData, "TIME BETWEEN CARDS\n");
        File.AppendAllText(fileLevelData, "LEVEL; CARD TIME\n");

        for (int i = 0; i < levelData.TimeBetweenPlacedCards.Count; i++)
        {
            for (int j = 0; j < levelData.TimeBetweenPlacedCards[i].Count; j++)
            {
                File.AppendAllText(fileLevelData, $"{i}; {levelData.TimeBetweenPlacedCards[i][j]}\n");
            }
        }

        //level times
        File.AppendAllText(fileLevelData, "LEVEL TIMES\n");
        File.AppendAllText(fileLevelData, "LEVEL; ELAPSED TIME\n");

        for (int i = 0; i < levelData.LevelTimes.Count; i++)
        {            
            File.AppendAllText(fileLevelData, $"{i}; {levelData.LevelTimes[i]}\n");            
        }

        //total time
        File.AppendAllText(fileLevelData, "TOTAL TIME\n");
        File.AppendAllText(fileLevelData, $"{levelData.TotalTime}");

        //cards grabbed and dropped
        File.AppendAllText(fileLevelData, "CARDS GRABBED\n");
        File.AppendAllText(fileLevelData, "LEVEL; ELAPSED TIME; CARD\n");

        for (int i = 0; i < levelData.CardsGrabbed.Count; i++)
        {
            File.AppendAllText(fileLevelData, $"{levelData.CardsGrabbedLevel[i]}; " +
                                              $"{levelData.TimesCardGrabbed[i]}; " +
                                              $"{levelData.CardsGrabbed[i]}\n");
        }

        File.AppendAllText(fileLevelData, "CARDS DROPPED\n");
        File.AppendAllText(fileLevelData, "LEVEL; ELAPSED TIME; CARD\n");

        for (int i = 0; i < levelData.CardsDropped.Count; i++)
        {
            File.AppendAllText(fileLevelData, $"{levelData.CardsDroppedLevel[i]}; " +
                                              $"{levelData.TimesCardDropped[i]}; " +
                                              $"{levelData.CardsDropped[i]}\n");
        }

    }
}
