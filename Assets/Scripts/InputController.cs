using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputController : MonoBehaviour
{
    public GameMetrics gameMetrics;

    public GameTimer gameTimer;

    //--- LEFT HAND XR ACTIONS --//
    public InputActionReference LHActivate = null;
    public InputActionReference LHMove = null;
    public InputActionReference LHRotation = null;
    public InputActionReference LHSelect = null;
    public InputActionReference LHTeleportModeActivate = null;
    public InputActionReference LHTeleportModeCancel = null;
    public InputActionReference LHTeleportSelect = null;
    public InputActionReference LHTurn = null;
    public InputActionReference LHUIPress = null;

    //--- RIGHT HAND XR ACTIONS --//
    public InputActionReference RHActivate = null;
    public InputActionReference RHMove = null;
    public InputActionReference RHRotation = null;
    public InputActionReference RHSelect = null;
    public InputActionReference RHTeleportModeActivate = null;
    public InputActionReference RHTeleportModeCancel = null;
    public InputActionReference RHTeleportSelect = null;
    public InputActionReference RHTurn = null;
    public InputActionReference RHUIPress = null;

    private void Awake()
    {
        LHActivate.action.started += SaveLHActivate;
        LHMove.action.started += SaveLHMove;
        LHRotation.action.started += SaveLHRotation;
        LHSelect.action.started += SaveLHSelect;
        LHTeleportModeActivate.action.started += SaveLHTeleportModeActivate;
        LHTeleportModeCancel.action.started += SaveLHTeleportModeCancel;
        LHTeleportSelect.action.started += SaveLHTeleportSeclect;
        LHTurn.action.started += SaveLHTurn;
        LHUIPress.action.started += SaveLHUIPress;

        RHActivate.action.started += SaveRHActivate;
        RHMove.action.started += SaveRHMove;
        RHRotation.action.started += SaveRHRotation;
        RHSelect.action.started += SaveRHSelect;
        RHTeleportModeActivate.action.started += SaveRHTeleportModeActivate;
        RHTeleportModeCancel.action.started += SaveRHTeleportModeCancel;
        RHTeleportSelect.action.started += SaveRHTeleportSeclect;
        RHTurn.action.started += SaveRHTurn;
        RHUIPress.action.started += SaveRHUIPress;
    }

    private void SaveRHUIPress(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("RHUIPress");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveRHTurn(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("RHTurn");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveRHTeleportSeclect(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("RHTeleportSelect");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveRHTeleportModeCancel(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("RHTeleportModeCancel");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveRHTeleportModeActivate(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("RHTeleportModeActivate");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveRHSelect(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("RHSelect");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveRHActivate(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("RHActivate");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveRHRotation(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("RHRotation");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveRHMove(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("RHMove");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveLHUIPress(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("LHUIPress");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveLHTurn(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("LHTurn");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveLHTeleportSeclect(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("LHTeleportSelect");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveLHTeleportModeCancel(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("LHTeleportModeCancel");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveLHTeleportModeActivate(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("LHTeleportModeActivate");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveLHSelect(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("LHSelect");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveLHRotation(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("LHRotation");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveLHMove(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("LHMove");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }

    private void SaveLHActivate(InputAction.CallbackContext obj)
    {
        if (gameMetrics == null || gameTimer == null)
            return;

        gameMetrics.ControllerActions.Add("LHActivate");
        gameMetrics.ControllerActionTimes.Add(gameTimer.TimeElapsed);
    }
        
    private void OnDestroy()
    {

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
