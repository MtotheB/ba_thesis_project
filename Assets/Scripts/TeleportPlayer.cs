using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

/// <summary>
/// Manually teleport the player to a specific anchor
/// Thanks to Andrew from VR with Andrew for this snippet!
/// <see href="https://www.patreon.com/VRwithAndrew"/>
/// </summary>
public class TeleportPlayer : MonoBehaviour
{
    [Tooltip("The anchor the player is teleported to")]
    public TeleportationAnchor anchor = null;

    [Tooltip("The provider used to request the teleportation")]
    public TeleportationProvider teleportation = null;

    public void Teleport()
    {
        if (anchor && teleportation)
        {            
            TeleportRequest request = CreateRequest();
            teleportation.QueueTeleportRequest(request);
        }
    }

    private TeleportRequest CreateRequest()
    {
        Transform anchorTransform = anchor.teleportAnchorTransform;

        TeleportRequest request = new TeleportRequest()
        {
            requestTime = Time.time,
            matchOrientation = anchor.matchOrientation,

            destinationPosition = anchorTransform.position,
            destinationRotation = anchorTransform.rotation
        };

        return request;
    }
}