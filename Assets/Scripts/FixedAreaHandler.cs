using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedAreaHandler : MonoBehaviour
{
    [SerializeField]
    [Tooltip("The shader material to be used for the fade.")]
    private Material material;

    [SerializeField]
    [Tooltip("The amount of transparency added or subtracted each frame.")]
    private float fadeStepOut;

    [SerializeField]
    [Tooltip("The amount of transparency added or subtracted each frame.")]
    private float fadeStepIn;

    [SerializeField]
    [Tooltip("Time between fadeout and fade in")]
    private float waitTime;

    private float timer = 0.0f;

    private bool isFadingOut;
    private bool isFadingIn;
    private bool isWaiting;
    private bool fadeDone;

    // Start is called before the first frame update
    void Start()
    {
        //reset material transparency...just in case :)
        if (material != null)
            material.SetFloat("_Transparency", 0); 
    }

    // Update is called once per frame
    void Update()
    {
        if (material == null)
            return;

        if (fadeDone)
            return;

        if (isFadingOut && material.GetFloat("_Transparency") < 1)
        {   
            float fadeAmount = material.GetFloat("_Transparency") + fadeStepOut;
            material.SetFloat("_Transparency", fadeAmount);

            if (material.GetFloat("_Transparency") >= 1)
            {               
                isFadingOut = false;
                isWaiting = true;
                //reset position at half way 
                GameController gameController = GameObject.Find("GameController").GetComponent<GameController>();
                gameController.ResetPlayerPosition();
                //isFadingIn = true;
            }
        }        

        if (isWaiting)
        {
            waitTime -= Time.deltaTime;

            if (waitTime <= 0.0f)
            {
                isWaiting = false;
                isFadingIn = true;
            }           
        }

        if (isFadingIn && material.GetFloat("_Transparency") > 0)
        {            
            float fadeAmount = material.GetFloat("_Transparency") - fadeStepIn;
            material.SetFloat("_Transparency", fadeAmount);

            if (material.GetFloat("_Transparency") <= 0)
            {                
                isFadingIn = false;
                fadeDone = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {        
        GameController gameController = GameObject.Find("GameController").GetComponent<GameController>();

        if (gameController == null)
            return;

        if (gameController.limitMovement)
        {
            //fade out and in
            FadeOutAndIn();            
        }
    }

    private void FadeOutAndIn()
    {        
        fadeDone = false;
        isFadingOut = true;
    }
}

   