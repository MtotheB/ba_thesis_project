using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameTimer : MonoBehaviour
{
    [Header("Timer")]
    [SerializeField]
    [Tooltip("Minutes before game ends.")]
    public float Minutes = 0;

    [SerializeField]
    [Tooltip("Seconds before game ends.")]
    public float Seconds = 59;

    [SerializeField]
    [Tooltip("Text Mesh that will be displayed.")]
    //public TextMeshPro TimerText;
    public TextMeshProUGUI TimerText;

    private bool started;
    private bool paused;
    private bool stopped;

    /// <summary>
    /// The time that has been elapsed
    /// so far in total, given in seconds.
    /// </summary>
    public float TimeElapsed { get; set; }

    /// <summary>
    /// The time that has been needed for
    /// the actual level.
    /// </summary>
    public float TimeLevel { get; set; }

    /// <summary>
    /// True, if the timer has been ran out.
    /// </summary>
    public bool TimerDone { get; set; }

    //---- TIME SAVES ----//

    /// <summary>
    /// Records the time that the player 
    /// needed for every level. Time will 
    /// be saved in seconds.
    /// </summary>
    public Dictionary<int, float> LevelTimes { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        LevelTimes = new Dictionary<int, float>();
    }

    // Update is called once per frame
    void Update()
    {
        if (paused || !started || stopped)
            return;
        
        if (Seconds <= 0)
        {
            Seconds = 59;
            if (Minutes >= 1)
            {
                Minutes--;
            }
            else
            {
                Minutes = 0;
                Seconds = 0;
                TimerDone = true;
            }
        }
        else
        {
            Seconds -= Time.deltaTime;
            TimeElapsed += Time.deltaTime;
            TimeLevel += Time.deltaTime;
        }

        TimerText.text = Minutes + " : " + (Seconds).ToString("00");
    }

    public void StartTimer()
    {
        Debug.Log("TIMER STARTED...");
        started = true;
        stopped = false;
        paused = false;
        TimerDone = false;
    }

    public void PauseTimer()
    {
        paused = true;
    }

    public void UnPauseTimer()
    {
        paused = false;
    }

    public void StopTimer()
    {
        stopped = true;
        started = false;
        Minutes = 0.0f;
        Seconds = 59.0f;
        TimeElapsed = 0.0f;
        TimeLevel = 0.0f;
        TimerDone = false;
    }

    /// <summary>
    /// Saves the time, that was needed
    /// for the actual level to be
    /// solved by the player.
    /// </summary>
    /// <param name="levelNumber">The number of the actual level.</param>
    public void SaveLevelTime(int levelNumber)
    {
        LevelTimes.Add(levelNumber, TimeLevel);
        TimeLevel = 0.0f;
    }
}
