using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SurveyData : MonoBehaviour
{
    //----- DATA LISTS -----//

    /// <summary>
    /// The list for saved survey data 
    /// about the mental effort slider
    /// per level.
    /// </summary>
    [HideInInspector]
    public List<int> MentalEffort;

    /// <summary>
    /// The list for saved survey data
    /// about the complexity per level.
    /// </summary>
    [HideInInspector]
    public List<int> Complexity;

    /// <summary>
    /// The list for saved final survey
    /// data about the difficulties
    /// of the cards in general.
    /// </summary>
    [HideInInspector]
    public List<int> CardDifficulties;
        
    /// <summary>
    /// The list for saved final survey
    /// data about the difficulties
    /// of the fields in general.
    /// </summary>
    [HideInInspector]
    public List<int> FieldDifficulties;

    /// <summary>
    /// List of survey data about the 
    /// difficulty of each correct 
    /// combination per level.
    /// </summary>
    public List<List<int>> CombinationDifficulty;

    /// <summary>
    /// The cards in the combinations.
    /// </summary>
    public List<List<string>> CombinationCards;

    /// <summary>
    /// The fields in the combinations
    /// </summary>
    public List<List<string>> CombinationFields;

    // Start is called before the first frame update
    void Start()
    {
        //initialize lists
        MentalEffort = new List<int>();
        Complexity = new List<int>();
        CardDifficulties = new List<int>();
        FieldDifficulties = new List<int>();
        CombinationDifficulty = new List<List<int>>();
        CombinationCards = new List<List<string>>();
        CombinationFields = new List<List<string>>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //----- HELPERS/DEBUG -----//

    /// <summary>
    /// For Debug purposes.
    /// </summary>
    public void PrintSurveyData()
    {
        Debug.Log("");
        Debug.Log("PRINTING LISTS OF SURVEY DATA: " + "\n");
        Debug.Log("");

        Debug.Log("MENTAL EFFORT:");
        PrintList(MentalEffort);
        Debug.Log("COMPLEXITY:");
        PrintList(Complexity);
        Debug.Log("CARD DIFFICULTIES");
        PrintList(CardDifficulties);
        Debug.Log("FIELD DIFFICULTIES");
        PrintList(FieldDifficulties);
        Debug.Log("COMBINATION DIFFICULTIES");
        PrintList(CombinationDifficulty);
    }

    /// <summary>
    /// Prints out a list to the debug console.
    /// </summary>
    private void PrintList<T>(List<T> listToPrint)
    {
        Debug.Log("List count: " + listToPrint.Count);        
        for (int i = 0; i < listToPrint.Count; i++)
        {
            Debug.Log("index " + i + ": " + listToPrint[i]);
        }        
    }

    /// <summary>
    /// Prints out a two-dimensional 
    /// list to the debug console.
    /// </summary>
    private void PrintList<T>(List<List<T>> listToPrint)
    {
        int h = listToPrint.Count;
        int w = listToPrint.Max(l => l.Count);

        Debug.Log("h: " + h + ", w: " + w);

        for (int i = 0; i < h; i++)
        {
            for (int j = 0; j < w; j++)
            {                
                if (j < listToPrint[i].Count)
                    Debug.Log("[" + i + "][" + j + "]: " + listToPrint[i][j]);
                else
                    Debug.Log(" ");
            }
            Debug.Log(" ");
        }        
    }
}
