using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCogWheels : MonoBehaviour
{
    [SerializeField]
    [Tooltip("The bigger wheel to rotate.")]
    private GameObject gearMain;
    [SerializeField]
    [Tooltip("The smaller wheel to rotate.")]
    private GameObject gearSmall;
    [SerializeField]
    [Tooltip("List of robo figures, that could be attached to the rotation of the main wheel.")]
    private List<GameObject> roboFigures;
    [SerializeField]
    [Tooltip("Time between rotation cycles.")]
    public float waitTime = 0.2f;
    [SerializeField]
    [Tooltip("Whether the wheels should rotate left or not (right then).")]
    public bool rotateLeft = true;

    /// <summary>
    /// The actual robo figure attached to this wheel.
    /// </summary>
    private GameObject attachedRobo;

    /// <summary>
    /// Timer for rotation of the wheels.
    /// </summary>
    private float timer = 0.0f;

    /// <summary>
    /// Whether a robo figure (out of the <see cref="roboFigures"/> List)
    /// is attached or not.
    /// </summary>
    private bool roboAttached = false;
    
    /// <summary>
    /// The starting angel of the attached robo figure.
    /// </summary>
    private float roboStartAngel = 0.0f;

    /// <summary>
    /// The actual angel of the attached robo figure.
    /// </summary>
    private float roboActualAngel = 0.0f;

    /// <summary>
    /// The max angel of the attached robo figure.
    /// </summary>
    private float roboMaxAngel = 90.0f;

    /// <summary>
    /// Whether or not the wheels should be spinning
    /// right now.
    /// </summary>
    public bool WheelsActive { get; set; }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!WheelsActive)
            return;

        timer += Time.deltaTime;

        if(timer > waitTime)
        {            
            int rotationMain = rotateLeft ? 1 : -1;
            int rotationSmall = rotateLeft ? -1 : 1;
            gearMain.transform.Rotate(0, 0, rotationMain);
            gearSmall.transform.Rotate(0, 0, rotationSmall);

            //works but lets do that by saved movement
            //in GameController for now
            if (roboAttached && attachedRobo != null )
            {
                if (roboActualAngel < roboMaxAngel)
                    attachedRobo.transform.Rotate(0, -rotationMain, 0);
            }

            timer -= waitTime;
        }        
    }

    private void OnTriggerEnter(Collider other)
    {   
        /*
        if (roboFigures.Contains(other.gameObject))
        {            
            attachedRobo = other.gameObject;
            roboStartAngel = attachedRobo.transform.eulerAngles.y;            
        }*/
    }

    void OnTriggerStay(Collider other)
    {
        /*
        if (roboFigures.Contains(other.gameObject))
        {
            roboAttached = true;
            roboActualAngel = attachedRobo.transform.eulerAngles.y;            
        }*/
    }

    private void OnTriggerExit(Collider other)
    {
        //probably needs better check later
        //if (roboFigures.Contains(other.gameObject))
            //roboAttached = false;
    }

}
