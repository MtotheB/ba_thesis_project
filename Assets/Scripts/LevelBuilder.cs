using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

/// <summary>
/// Handles the level creation.
/// </summary>
public class LevelBuilder : MonoBehaviour
{
    //--- EDITOR (INSPECTOR) FIELDS ---//
    [Header("Game Objects")]
    [SerializeField]
    [Tooltip("The GameController script for this game.")]
    GameController gameController;

    [SerializeField]
    [Tooltip("To store levels and other data related.")]
    LevelData levelData;

    [SerializeField]
    [Tooltip("The game area on the table.")]
    private GameObject gameArea;

    [SerializeField]
    [Tooltip("Game field Rotate Right.")]
    private GameObject gameFieldRotateRight;
    [SerializeField]
    [Tooltip("Game field Rotate Left.")]
    private GameObject gameFieldRotateLeft;
    [SerializeField]
    [Tooltip("Game field Conveyor Forward.")]
    private GameObject gameFieldConveyorSimple;
    [SerializeField]
    [Tooltip("Game field Conveyor Forward without animation.")]
    private GameObject gameFieldConveyorSimpleStill;
    [SerializeField]
    [Tooltip("Game field Conveyor Double Forward.")]
    private GameObject gameFieldConveyorDouble;
    [SerializeField]
    [Tooltip("Game field Conveyor Double Forward without animation.")]
    private GameObject gameFieldConveyorDoubleStill;
    [SerializeField]
    [Tooltip("Game field Goal.")]
    private GameObject gameFieldGoal;
    [SerializeField]
    [Tooltip("Game field Start.")]
    private GameObject gameFieldStart;

    [SerializeField]
    [Tooltip("Normal game fields.")]
    private List<GameObject> normalGameFields;

    [SerializeField]
    [Tooltip("Color for darkened fields")]
    private Color darkNormalFieldColor;

    [SerializeField]
    [Tooltip("Color for normal fields")]
    private Color normalFieldColor;
   
    [SerializeField]
    [Tooltip("The available robo models.")]
    private List<GameObject> roboModels;

    [Header("Programming Cards")]
    [SerializeField]
    private GameObject CardMove1;
    [SerializeField]
    private GameObject CardMove2;
    [SerializeField]
    private GameObject CardMove3;
    [SerializeField]
    private GameObject CardMoveBack;
    [SerializeField]
    private GameObject CardRotateRight;
    [SerializeField]
    private GameObject CardRotateLeft;
    [SerializeField]
    private GameObject CardUTurn;

    [SerializeField]
    [Tooltip("Placeholders for programming cards")]
    private List<GameObject> cardPlaceholders;
    
    [Header("Game Area Definitions")]
    [SerializeField]
    [Tooltip("The number of game field tiles in horizontal direction.")]
    private int gameAreaWidth = 12;
    [SerializeField]
    [Tooltip("The number of game field tiles in vertical direction.")]
    private int gameAreaHeight = 12;
    [SerializeField]
    [Tooltip("The row of the start tile where the robot will be placed initially. Note: The smallest row starts at 0!")]
    private int startTileRow = 6;
    [SerializeField]
    [Tooltip("The column of the start tile where the robot will be placed initially. Note: The smallest column starts at 0!")]
    private int startTileCol = 6;

    //save
    private int originalStartRow;
    private int originalStartCol;

    [Header("Probabilities Field Types (should add up to 100)")]
    [SerializeField]
    [Range(0, 100)]
    private int rotateRightFieldProbability;
    [SerializeField]
    [Range(0, 100)]
    private int rotateLeftFieldProbability;
    [SerializeField]
    [Range(0, 100)]
    private int simpleConveyorProbability;
    [SerializeField]
    [Range(0, 100)]
    private int doubleConveyorProbability;
    [SerializeField]
    [Range(0, 100)]
    private int normalFieldProbability;

    [Header("Sum Field Probabilities")]
    [SerializeField]
    private int sumProbsFields;

    [Header("Probabilities Card Types (should add up to 100)")]
    [SerializeField]
    [Range(0, 100)]
    private int cardMove1Probability;
    [SerializeField]
    [Range(0, 100)]
    private int cardMove2Probability;
    [SerializeField]
    [Range(0, 100)]
    private int cardMove3Probability;
    [SerializeField]
    [Range(0, 100)]
    private int cardUTurnProbability;
    [SerializeField]
    [Range(0, 100)]
    private int cardRRightProbability;
    [SerializeField]
    [Range(0, 100)]
    private int cardRLeftProbability;
    [SerializeField]
    [Range(0, 100)]
    private int cardBackProbability;

    [Header("Sum Card Probabilities")]
    [SerializeField]
    private int sumProbsCards;

    ///--- EDITOR STUFF ---///
    private int rRFieldProbLast = 0;
    private int rLFieldProbLast = 0;
    private int simpleConvFieldProbLast = 0;
    private int doubleConvFieldProbLast = 0;
    private int normalFieldProbLast = 0;
    private int cardM1ProbLast = 0;
    private int cardM2ProbLast = 0;
    private int cardM3ProbLast = 0;
    private int cardUTurnProbLast = 0;
    private int cardRRProbLast = 0;
    private int cardRLProbLast = 0;
    private int cardBackProbLast = 0;

    [Header("Info Text")]
    [TextArea(10, 15)]
    public string debugInfo= "";    
    
    ///--- PRIVATE ---///
    /// <summary>
    /// Maximmum possible number of tiles in width.
    /// </summary>
    private int maxTilesWidth;

    /// <summary>
    /// Maximum possible number of tiles in height.
    /// </summary>
    private int maxTilesHeight;

    /// <summary>
    /// This array holds all the placed game field (objects).
    /// </summary>
    private GameObject[,] fieldMatrix;


    //---INTERNAL DEFINES---//
    //Start orientation
    //0 -> looks right (3 o clock)
    //1 -> looks down (6 o clock)
    //2 -> looks left (9 o clock)
    //3 -> looks up (12 o clock)
    private static int START_ORIENTATION = 0;
    [HideInInspector]
    public int NUMBER_ORIENTATIONS = 4;

    //TODO: Instead of fix virtual array use
    //      lists to make this bit more dynamic.
    /// <summary>
    /// Size of the temporary matrix.
    /// </summary>
    private static int VIRT_MATRIX_ROWS = 40;
    private static int VIRT_MATRIX_COLS = 40;

    //Number of commands to fulfill level
    private static int MAX_COMMANDS = 5;

    /// <summary>
    /// Holds references for the game fields for
    /// the level creation.
    /// </summary>
    public Dictionary<string, GameObject> GameFields;

    /// <summary>
    /// Holds references for the programming card types.
    /// </summary>
    private Dictionary<string, GameObject> ProgrammingCards;

    /// <summary>
    /// Connection for the game field byte representations
    /// and their string counterpart.
    /// </summary>
    public Dictionary<byte, string> PredefinedGameFields;

    /// <summary>
    /// Connection for the game field byte
    /// representation at runtime.
    /// </summary>
    private Dictionary<string, byte> FieldByteRepresentation;

    /// <summary>
    /// Here the spawn probablities for certain fame fields are set.
    /// </summary>
    private List<KeyValuePair<string, float>> FieldProbabilities;

    /// <summary>
    /// Here the spawn probablities for certain card types are set.
    /// </summary>
    private List<KeyValuePair<string, float>> CardProbabilities;

    /// <summary>
    /// Here the spawn probablities for the field types 
    /// for the starting combination are set.
    /// </summary>
    private List<KeyValuePair<string, float>> FieldProbabilitiesStart;

    /// <summary>
    /// Here the spawn probablities for the card types 
    /// for the starting combination are set.
    /// </summary>
    private List<KeyValuePair<string, float>> CardProbabilitiesStart;

    [HideInInspector]
    /// <summary>
    /// Saved combinations of card and field commands.
    /// </summary>
    public List<string> Combinations;

    /// <summary>
    /// List to save all fields the robo walks on.
    /// </summary>
    private List<(int, int)> WalkedFieldCoords;

    /// <summary>
    /// A List holding the coords of every game field placed.
    /// </summary>
    private List<(int, int)> PlacedFieldCoords;

    /// <summary>
    /// A List holding references to the tiles
    /// that have been placed.
    /// </summary>
    private List<GameObject> GeneratedFields;

    /// <summary>
    /// List to save all orientations of generated fields.
    /// </summary>
    private List<int> GeneratedRotations;

    /// <summary>
    /// Keeps track of which card placeholders were filled already.
    /// </summary>
    private List<int> FilledPlaceholders;

    /// <summary>
    /// Lists all the invalid field type <> card type combinations.
    /// </summary>
    private static List<string> WrongCombinations = new List<string>()
    {
        {"C_B-F_RR.C_M1-F_F1"},
        {"C_B-F_RR.C_M1-F_F2"},
        {"C_B-F_RR.C_M2-F_F1"},
        {"C_B-F_RR.C_M2-F_F2"},
        {"C_B-F_RR.C_M3-F_F1"},
        {"C_B-F_RR.C_M3-F_F2"},
        {"C_B-F_RR.C_B-F_F1"},
        {"C_B-F_RR.C_B-F_F2"},
        {"C_B-F_RL.C_M1-F_F1"},
        {"C_B-F_RL.C_M1-F_F2"},
        {"C_B-F_RL.C_M2-F_F1"},
        {"C_B-F_RL.C_M2-F_F2"},
        {"C_B-F_RL.C_M3-F_F1"},
        {"C_B-F_RL.C_M3-F_F2"},
        {"C_B-F_RL.C_B-F_F1"},
        {"C_B-F_RL.C_B-F_F2"},
        {"C_B-F_N.C_M1-F_F1"},
        {"C_B-F_N.C_M1-F_F2"},
        {"C_B-F_N.C_M2-F_F1"},
        {"C_B-F_N.C_M2-F_F2"},
        {"C_B-F_N.C_M3-F_F1"},
        {"C_B-F_N.C_M3-F_F2"},
        {"C_B-F_N.C_B-F_F1"},
        {"C_B-F_N.C_B-F_F2"},
        {"C_B-F_F1.C_M1-F_F1"},
        {"C_B-F_F1.C_M1-F_F2"},
        {"C_B-F_F1.C_M2-F_F1"},
        {"C_B-F_F1.C_M2-F_F2"},
        {"C_B-F_F1.C_M3-F_F1"},
        {"C_B-F_F1.C_M3-F_F2"},
        {"C_B-F_F1.C_B-F_F1"},
        {"C_B-F_F1.C_B-F_F2"},
        {"C_B-F_F2.C_M1-F_F1"},
        {"C_B-F_F2.C_M1-F_F2"},
        {"C_B-F_F2.C_M2-F_F1"},
        {"C_B-F_F2.C_M2-F_F2"},
        {"C_B-F_F2.C_M3-F_F1"},
        {"C_B-F_F2.C_M3-F_F2"},
        {"C_B-F_F2.C_B-F_F1"},
        {"C_B-F_F2.C_B-F_F2"}
    };

    //----- Numerical representations of field types ------ //    
    private static byte standardFieldDark = 0;
    private static byte standardFieldNorm = 1;
    private static byte convFieldSimpleStill = 2;
    private static byte convFieldSimple = 3;
    private static byte convFieldDoubleStill = 4;
    private static byte convFieldDouble = 5;
    private static byte rotateFieldRight = 6;
    private static byte rotateFieldLeft = 7;
    private static byte goalField = 8;
    private static byte startField = 9;

    /// <summary>
    /// An array of predefined levels.
    /// Note:   For double conveyor fields only place one 
    ///         corresponding byte. The second field will
    ///         later be added via the algorithm in
    ///         <see cref="PlaceField(string, int)"/>.
    /// </summary>
    private byte[,,] PredefinedFieldMatrices = 
    {
        {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 9, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 4, 1, 1, 8, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        },

        {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 6, 1, 3, 1, 8, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 9, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        },
    };

    /// <summary>
    /// The corresponding orientations of the fields 
    /// saved in <see cref="PredefinedFieldMatrices"/>.
    /// </summary>
    private byte[,,] PredefinedFieldOrientations =
    {
         {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        },

        {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        },
    };

    /// <summary>
    /// For tutorial. Holds the cards to place for the 
    /// predefined tutorial levels.
    /// </summary>
    private string[,] PredefinedCardList = 
    {
        {"C_M3", "C_RR", "C_M1", "C_RL", "C_M1"},
        {"C_M1", "C_RL", "C_M2", "C_M2", "C_M1"}
    };

    /// <summary>
    /// List to hold reference to certain game objects.
    /// </summary>
    private List<GameObject> CreatedGameObjects;

    /// <summary>
    /// List to check whether cards should be
    /// shuffled for a predefined level or not.
    /// </summary>
    private List<bool> PredefinedLevelCardShuffle = new List<bool>()
    {
         false,
         true
    };
        
    /// <summary>
    /// The actual orientation of the robot.
    /// </summary>
    private int actualOrientation;

    /// <summary>
    /// The direction the robo faces at the start.
    /// </summary>
    private int startOrientation;

    /// <summary>
    /// The actual row, where the robot is.
    /// </summary>
    public int actualRow;

    /// <summary>
    /// The actual column, where the robot is.
    /// </summary>
    private int actualCol;

    /// <summary>
    /// Number of movements by the program cards.
    /// </summary>
    private int numMovesCard;

    /// <summary>
    /// Number of movements by the game fields.
    /// </summary>
    private int numMovesField;
     
    /// <summary>
    /// The robo player figure.
    /// </summary>
    [HideInInspector]
    public GameObject robo;

    /// <summary>
    /// In case some tiles exceed the
    /// game area set this to true.
    /// </summary>
    private bool wasOutOfGameArea;

    /// <summary>
    /// Temporary matrix to store level first.
    /// </summary>
    private GameObject[,] virtualFieldMatrix;

    /// <summary>
    /// Holds positions to where to place
    /// the programming cards.
    /// </summary>
    private List<Vector3> CardPlaceHoldersPos;

    //--- EXPORT ---//

    /// <summary>
    /// To share with GameController.
    /// </summary>
    public int ActualRow 
    { 
        get => actualRow; 
    }

    /// <summary>
    /// To share with GameController.
    /// </summary>
    public int ActualCol 
    { 
        get => actualCol; 
    }

    /// <summary>
    /// To share with GameController.
    /// </summary>
    public int StartRow { get; set; }

    /// <summary>
    /// To share with GameController.
    /// </summary>
    public int StartCol { get; set; }    

    /// <summary>
    /// To share with LevelData.
    /// </summary>
    public (int, int) FieldDimensions 
    {
        get => (gameAreaWidth, gameAreaHeight);
    }

    /// <summary>
    /// To share with GameController.
    /// </summary>
    public int NumberProgrammingCards 
    { 
        get => cardPlaceholders.Count; 
    }

    /// <summary>
    /// To share with GameController.
    /// Allow setting/changing fields from GC.
    /// </summary>
    public GameObject[,] FieldMatrix 
    { 
        get => fieldMatrix;
        set => fieldMatrix = value;
    }

    /// <summary>
    /// To share with GameController.
    /// </summary>
    public List<(int, int)> WalkedFields 
    {
        get => WalkedFieldCoords;            
    }

    /// <summary>
    /// To share with GameController.
    /// </summary>
    public List<(int, int)> PlacedFields
    {
        get => PlacedFieldCoords;
    }

    /// <summary>
    /// To share with GameController.
    /// </summary>
    [HideInInspector]
    public List<int> SavedOrientations;

    /// <summary>
    /// To share with GameController.
    /// </summary>
    public List<(int, int)> SavedCoords;

    /// <summary>
    /// To share with GameController.
    /// </summary>
    [HideInInspector]
    public List<bool> Rotated;

    /// <summary>
    /// To share with GameController.
    /// </summary>
    public int StartOrientation 
    {
        get => startOrientation; 
    }

    /// <summary>
    /// To share with GameController.
    /// </summary>
    public Dictionary<string, GameObject> GameFieldsDict
    {
        get => GameFields;
    }

    /// <summary>
    /// To share with GameController.
    /// </summary>
    public List<string> SavedCombinations 
    {
        get => Combinations;
    }

    /// <summary>
    /// To share with GameController.
    /// </summary>
    public int NumTutorialLevels { get; set; }

    /// <summary>
    /// To share with ImportController.
    /// </summary>
    public byte StartFieldRepresentation 
    {
        get => startField;
    }

    /// <summary>
    /// To share with ImportController.
    /// </summary>
    public byte GoalFieldRepresentation
    {
        get => goalField;
    }

    public byte StandardFieldWalkedRepresentation
    {
        get => standardFieldNorm;
    }

    //--- START ---///
    void Start()
    {
        //initialize lists
        Combinations = new List<string>();
        FilledPlaceholders = new List<int>();
        WalkedFieldCoords = new List<(int, int)>();
        PlacedFieldCoords = new List<(int, int)>();
        SavedCoords = new List<(int, int)>();
        SavedOrientations = new List<int>();
        GeneratedFields = new List<GameObject>();
        CreatedGameObjects = new List<GameObject>();
        GeneratedRotations = new List<int>();
        CardPlaceHoldersPos = new List<Vector3>();
        //initialize temporary matrix
        virtualFieldMatrix = new GameObject[VIRT_MATRIX_ROWS, VIRT_MATRIX_COLS];
        //save initial values of field probs 
        SaveProbValues();
        //store game field shortcuts
        StoreGameFieldTypes();
        //store field- & card-spawn-probabilities
        StoreProbabilities();
        //assign card types to card types strings
        AssignProgramCards();
        //saves
        StartRow = startTileRow;
        StartCol = startTileCol;
        originalStartRow = startTileRow;
        originalStartCol = startTileCol;
        startOrientation = 0;

        //save card positions
        for (int i = 0; i < cardPlaceholders.Count; i++)
        {
            Vector3 pos = cardPlaceholders[i].transform.position;
            CardPlaceHoldersPos.Add(pos);
        }

        //set number of predefined levels
        NumTutorialLevels = 2;
    }

    ///--- UPDATE ---///
    void Update()
    {

    }

    /// <summary>
    /// Gets called when some value got changed.
    /// Used to check if slidersSurveyCombinations for field/card-spawn
    /// probablities are changed.
    /// </summary>
    private void OnValidate()
    {
        ///--- EDITOR STUFF ---///
        sumProbsFields = simpleConveyorProbability + doubleConveyorProbability +
                         normalFieldProbability + rotateRightFieldProbability +
                         rotateLeftFieldProbability;

        //check which field prob was changed if sum is >100
        if (sumProbsFields > 100)
        {
            //reset slider (value) back so it 
            //doesn't exceed 100 in sum
            if (rRFieldProbLast != rotateLeftFieldProbability)
                rotateRightFieldProbability = rRFieldProbLast;
            if (rLFieldProbLast != rotateLeftFieldProbability)
                rotateLeftFieldProbability = rLFieldProbLast;
            if (simpleConvFieldProbLast != simpleConveyorProbability)
                simpleConveyorProbability = simpleConvFieldProbLast;
            if (doubleConvFieldProbLast != doubleConveyorProbability)
                doubleConveyorProbability = doubleConvFieldProbLast;
            if (normalFieldProbLast != normalFieldProbability)
                normalFieldProbability = normalFieldProbLast;

            sumProbsFields = 100;
        }

        sumProbsCards = cardMove1Probability + cardMove2Probability +
                        cardMove3Probability + cardRRightProbability +
                        cardRLeftProbability + cardUTurnProbability +
                        cardBackProbability;

        if (sumProbsCards > 100)
        {
            if (cardM1ProbLast != cardMove1Probability)
                cardMove1Probability = cardM1ProbLast;
            if (cardM2ProbLast != cardMove2Probability)
                cardMove2Probability = cardM2ProbLast;
            if (cardM3ProbLast != cardMove3Probability)
                cardMove3Probability = cardM3ProbLast;
            if (cardRLProbLast != cardRLeftProbability)
                cardRLeftProbability = cardRLProbLast;
            if (cardRRProbLast != cardRRightProbability)
                cardRRightProbability = cardRRProbLast;
            if (cardUTurnProbLast != cardUTurnProbability)
                cardUTurnProbability = cardUTurnProbLast;
            if (cardBackProbLast != cardBackProbability)
                cardBackProbability = cardBackProbLast;

            sumProbsCards = 100;
        }

        //save last value
        SaveProbValues();
    }

    /// <summary>
    /// Saves the actual values for field- and card-
    /// probabilities for later comparison.
    /// </summary>
    private void SaveProbValues()
    {
        //fields
        rRFieldProbLast = rotateRightFieldProbability;
        rLFieldProbLast = rotateLeftFieldProbability;
        simpleConvFieldProbLast = simpleConveyorProbability;
        doubleConvFieldProbLast = doubleConveyorProbability;
        normalFieldProbLast = normalFieldProbability;
        //cards
        cardM1ProbLast = cardMove1Probability;
        cardM2ProbLast = cardMove2Probability;
        cardM3ProbLast = cardMove3Probability;
        cardRRProbLast = cardRRightProbability;
        cardRLProbLast = cardRLeftProbability;
        cardUTurnProbLast = cardUTurnProbability;
        cardBackProbLast = cardBackProbability;
    }

    //--- GAME HANDLING ---//

    /// <summary>
    /// Prepares the initial game field.
    /// </summary>
    public void PrepareGameField()
    {
        //Check for missing tiles in editor.
        if (!AllTilesAssigned())
            return;
        //Check entered dimensions for tiles in horizontal 
        //and vertical direction is too big for game field.
        CheckGameFieldDimensions();
        //prepare game field
        PlaceNormTiles();
        //test
        if (gameController.debug)
            TestShowGridCoords();
    }

    /// <summary>
    /// Resets the actual level.
    /// </summary>
    public void ResetLevel()
    {
        Destroy(robo);
        ResetGameField();
        ResetCards();
        ResetLists();
        CreateLevel();
    }

    /// <summary>
    /// Resets the game field with norm tiles.
    /// </summary>
    public void ResetGameField()
    {
        for (int i = 0; i < fieldMatrix.GetLength(0); i++)
            for (int j = 0; j < fieldMatrix.GetLength(1); j++)
                ReplaceTileWithNormTile(i, j);
    }

    /// <summary>
    /// Resets the cards and sets back the card slots.
    /// </summary>
    public void ResetCards()
    {
        FilledPlaceholders.Clear();

        for (int i = 0; i < cardPlaceholders.Count; i++)
        {
            if (cardPlaceholders[i] != null)
                Destroy(cardPlaceholders[i]);
        }
    }

    /// <summary>
    /// Clears all relevant lists.
    /// </summary>
    public void ResetLists()
    {
        WalkedFieldCoords.Clear();
        Rotated.Clear();
        SavedCoords.Clear();
        SavedOrientations.Clear();
        Combinations.Clear();
    }

    /// <summary>
    /// Destroys all <see cref="GameObject"/>s
    /// with a certain tag.
    /// </summary>    
    /// <param name="tag">The tag of the <see cref="GameObject"/>s to be destroyed.</param>
    public void DestroyGameObjectsWithTag(string tag)
    {
        GameObject[] toDestroy = GameObject.FindGameObjectsWithTag(tag);

        foreach (var gObj in toDestroy)
        {
            Destroy(gObj);
        }
    }

    /// <summary>
    /// EXPORT to call via <see cref="GameController"/>.
    /// </summary>
    /// <param name="number">The number of the predefined level to create.</param>
    public void CreateTutorial(byte number)
    {
        if (number >= PredefinedFieldMatrices.GetLength(0))
            return;

        if(number > 0)
        {
            Destroy(robo);
            ResetCards();
            DestroyGameObjectsWithTag("GameField");            
            ResetGameField();

            SocketController sc = GameObject.Find("SocketController").GetComponent<SocketController>();

            if (sc != null)
                sc.ResetSockets();

            /*
            foreach (var field in CreatedGameObjects)
            {
                Destroy(field);
            }*/
        }

        CreateLevel(GetPredefinedMatrix(PredefinedFieldMatrices, number), 
                    GetPredefinedMatrix(PredefinedFieldOrientations, number), 
                    GetPredefinedArray(PredefinedCardList, number), PredefinedLevelCardShuffle[number]);        
    }

    public void SafeResetField()
    {
        Destroy(robo);
        ResetCards();
        DestroyGameObjectsWithTag("GameField");
        ResetGameField();

        SocketController sc = GameObject.Find("SocketController").GetComponent<SocketController>();

        if (sc != null)
            sc.ResetSockets();
    }
    
    /// <summary>
    /// Creates a predefined level.
    /// This method needs a predefined matrix, representing
    /// the game fields by numbers (<see cref="PredefinedFieldMatrices"/>),
    /// a corresponding matrix, holding the orientations of those fields
    /// as well as a list of the correct programming cards
    /// for this level. The cards should be in the correct
    /// order for the robo to process this level.
    /// However, they can be shuffled by passing the optional
    /// parameter.
    /// </summary>
    /// <param name="fields">The matrix holding references to the game fields to be placed.</param>
    /// <param name="fieldOrientations">The matrix holding the orientations for the game fields.</param>
    /// <param name="cards">The cards to be placed for this level.</param>
    /// <param name="shuffleCards">(Optional) Whether to shuffle the programming cards or not.</param>
    public void CreateLevel(byte[,] fields, byte[,] fieldOrientations, string[] cards, bool shuffleCards = false)
    {
        //first get start and goal field coords
        (int, int) startFieldCoords = GetCoordsOfField(fields, startField);
        (int, int) goalFieldCoords = GetCoordsOfField(fields, goalField);
        //overwrite startfield coords
        startTileRow = startFieldCoords.Item1;
        startTileCol = startFieldCoords.Item2;
        //also update properties for export
        StartRow = startTileRow;
        StartCol = startTileCol;
        //place the start field
        PlaceStartField();
        //place robo on start field
        PlaceRobo();

        //place all other fields
        for (int i = 0; i < fields.GetLength(0); i++)
        {
            for (int j = 0; j < fields.GetLength(1); j++)
            {
                //assign actual row/col
                actualRow = i;
                actualCol = j;
                //place field
                if (PredefinedGameFields.TryGetValue(fields[i, j], out string partField))
                {
                    PlaceField(partField, fieldOrientations[i, j], true);
                    //highlight way
                    if (partField == "F_N_H")
                        HighlightField((i, j));
                }
            }
        }

        //go through cards
        for (int i = 0; i < cards.Length; i++)
            PlaceCard(cards[i], i, !shuffleCards);        
    }
    
    /// <summary>
    /// Creates the actual level (problem) to be solved.    
    /// This is the regular method to use. The overload
    /// version is for creating predefined (or loaded)
    /// levels!
    /// </summary>
    public void CreateLevel()
    {
        //reset bools
        wasOutOfGameArea = false;
        //reset start tile row/col
        startTileRow = originalStartRow;
        startTileCol = originalStartCol;
        //also reset properties
        StartRow = startTileRow;
        StartCol = startTileCol;

        int numCreatedCommands = 0;
        int prevRow = 0;
        int prevCol = 0;
        int prevOrientation = 0;
        string prevComb = "";

        //assign start orientation
        actualOrientation = START_ORIENTATION;

        //save lastOrientation temporary
        int lastOrientation = actualOrientation;

        //assign start row/col
        actualRow = startTileRow;
        actualCol = startTileCol;

        //place the start field
        PlaceStartField();

        //place robo on start field
        PlaceRobo();

        while(numCreatedCommands < MAX_COMMANDS)
        {
            //randomly choose combination of card and field type
            //according to their probability            
            string rndFieldStr = "";
            string rndCardStr = "";

            //last command = goal
            if (numCreatedCommands == MAX_COMMANDS - 1)
                rndFieldStr = "F_G";
            else
                rndFieldStr = GetRndStr(numCreatedCommands, false);

            rndCardStr = GetRndStr(numCreatedCommands, true);

            string comb = rndCardStr + "-" + rndFieldStr;

            debugInfo += "\n";
            debugInfo += "CARD-/FIELD-COMBINATION " + (numCreatedCommands + 1) + ": " + comb + "\n";

            //save actual row and col for field rotation check
            prevRow = actualRow;
            prevCol = actualCol;
            //save prev orientation
            prevOrientation = actualOrientation;

            //now check if combination of actual command
            //and previous one is allowed
            string combToCheck = comb + "." + prevComb;

            //split card and field command
            int strPos = comb.IndexOf("-");
            int fieldRotation = 0;
            string partCard = comb.Substring(0, strPos);
            string partField = comb.Substring(strPos + 1);

            strPos = prevComb.IndexOf("-");
            string prevPartField = prevComb.Substring(strPos + 1);

            //if prev. field was a rotation field and actual card
            //is also about rotation/u-turn, prevent it
            bool doubleRotation = false;

            if ((prevPartField == "F_RR" || prevPartField == "F_RL") &&
                (partCard == "C_RR" || partCard == "C_RL" || partCard == "C_UT"))
                doubleRotation = true;

            //save previous combination for later
            prevComb = comb;

            if ((!WrongCombinations.Contains(combToCheck) && !doubleRotation))
            {
                //debugInfo += "COMBINATION IS VALID! STARTING PLACEMENT..." + "\n";
                //debugInfo += "partCard: " + partCard + ", partField: " + partField + "\n";

                //if card is rotation or UTurn set field to empty
                if (partCard == "C_RL" || partCard == "C_RR" || partCard == "C_UT")
                {
                    partField = "";
                    comb = partCard;
                }
                
                //PlaceCard(partCard, numCreatedCommands, gameController.debug);
                                
                //get number of moves from card and field
                numMovesCard = GetNumberOfMovesCard(partCard);
                numMovesField = GetNumberOfMovesField(partField);

                //process the movement of the robo
                //according to the card... 
                ProcessMovementCard();
                
                //If tile can't be placed rewoke all
                //changes and continue loop.
                if (!IsOutOfIndex(fieldMatrix, actualRow, actualCol))
                {
                    if(IsFieldOccupied((actualRow, actualCol)))
                    {   
                        actualRow = prevRow;
                        actualCol = prevCol;                                                
                        continue;                        
                    }
                }

                PlaceCard(partCard, numCreatedCommands, gameController.debug);

                //...and set orientation
                //in case the card was rotation command
                actualOrientation = GetOrientationCard(partCard);

                //save rotation change
                bool rotated = lastOrientation != actualOrientation;
                Rotated.Add(rotated);

                lastOrientation = actualOrientation;

                bool samePosition = (prevRow == actualRow) && (prevCol == actualCol);
                
                //prevent to place over another field
                if (samePosition && !rotated)
                    continue;
                
                SaveMovement();

                bool isConveyor = false;

                //if field is conveyor 1x or 2x rotate it to random orientation
                //but prevent that it faces the direction from 
                //which the robo comes atm
                if (partField == "F_F1" || partField == "F_F2")
                {
                    fieldRotation = GetFieldRotation(prevRow, prevCol);
                    isConveyor = true;
                }
                                
                //place the field                
                PlaceField(partField, fieldRotation);

                //save the field first, placing later!
                //SaveField(partField, fieldRotation);
                
                prevRow = actualRow;
                prevCol = actualCol;

                //process the movement of the field...
                ProcessMovementField(fieldRotation, isConveyor);

                //...or change orientation if rotation field
                actualOrientation = GetOrientationField(partField);

                rotated = lastOrientation != actualOrientation;
                Rotated.Add(rotated);

                lastOrientation = actualOrientation;

                //prevent to place over another field
                //if (samePosition && !rotated)
                    //continue;

                //SaveMovement();

                //prevent to save same move
                if (!(samePosition && !rotated))
                    SaveMovement();                

                numCreatedCommands += 1;
                Combinations.Add(comb);
            }
            else
            {
                //continue while loop...
                debugInfo += "COMMAND NOT ALLOWED IN COMBINATION WITH OLD ONE" + "\n";
            }
        }

        if (wasOutOfGameArea)
        {
            gameController.ResetField();            
            //FixTiles();
        }

        //cut virtual matrix and transfer
        //game fields to real fieldMatrix
        //TransferFieldsToMatrix();
        //place saved game fields
        //PlaceFieldTiles();

        //finally highlight the right ones
        HighlightFields();

        //CleanSavedMovements();

        //debug placed tiles etc.
        if (gameController.debug)
            DebugLists();
    }
    
    /// <summary>
    /// Saves a game field, that shall be placed later
    /// into a the <see cref="virtualFieldMatrix"/>.
    /// </summary>
    private void SaveField(string partField, int rotation)
    {
        /*debugInfo += "\n";
        debugInfo += "Saving field of type: " + partField + " and with rotation: " + rotation
                  + " at actualCol: " + actualCol + " and actualRow: " + actualRow 
                  + " into virtual matrix" + "\n";*/

        if (partField == "")
            return;

        GameObject fieldToPlace;

        //try to get correct game field
        if (!GameFields.TryGetValue(partField, out fieldToPlace))
            return;

        //rotate field into correct orientation
        fieldToPlace.transform.rotation = Quaternion.Euler(0, (rotation * 90) + 90, 0);

        //save into virtual array 
        virtualFieldMatrix[actualRow, actualCol] = fieldToPlace;

        //if field is forward 2x place another one next to it
        if (partField == "F_F2")
        {
            int rowScndField = actualRow;
            int colScndField = actualCol;

            switch (rotation)
            {
                case 0:
                    colScndField += 1;
                    break;
                case 1:
                    rowScndField += 1;
                    break;
                case 2:
                    colScndField -= 1;
                    break;
                case 3:
                    rowScndField -= 1;
                    break;
                default:
                    break;
            }

            virtualFieldMatrix[rowScndField, colScndField] = fieldToPlace;
        }
    }

    /// <summary>
    /// Transfers all game fields to the real 
    /// field matrix.
    /// </summary>
    private void TransferFieldsToMatrix()
    {        
        //cut virtual matrix to correct size                
        virtualFieldMatrix = CutVirtualMatrix();

        //now transfer tiles to fieldmatrix
        //could be just replaced by cutted matrix
        //but position of tiles is not saved
        for (int i = 0; i < virtualFieldMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < virtualFieldMatrix.GetLength(1); j++)
            {
                //only replace if not a norm tile
                if(!virtualFieldMatrix[i,j].name.Contains("Standard"))
                {
                    Vector3 pos = fieldMatrix[i, j].transform.position;
                    /*debugInfo += "Replacing tile: " + fieldMatrix[i, j].name + " with: " + virtualFieldMatrix[i, j].name;
                    debugInfo += "at position: " + pos;
                    debugInfo += "\n";*/
                    Destroy(fieldMatrix[i, j]);
                    fieldMatrix[i, j] = virtualFieldMatrix[i, j];
                    fieldMatrix[i, j].transform.position = pos;
                }
            }
        }        
    }

    /// <summary>
    /// Places the game fields onto the board.
    /// </summary>
    private void PlaceFieldTiles()
    {
        /*debugInfo += "\n";
        debugInfo += "Placing tiles. Size of field matrix: " + fieldMatrix.GetLength(0) + ", " + fieldMatrix.GetLength(1);
        debugInfo += "\n";*/
        for (int i = 0; i < fieldMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < fieldMatrix.GetLength(1); j++)
            {
                if (!fieldMatrix[i, j].name.Contains("Standard"))
                {
                    Vector3 pos = fieldMatrix[i, j].transform.position;
                    Instantiate(fieldMatrix[i, j], pos, Quaternion.identity);

                    /*debugInfo += "Placing tile: " + fieldMatrix[i, j].name + " at position: " + pos;
                    debugInfo += "fieldMatrix[" + i + ", " + j + "]: " + fieldMatrix[i, j].name;
                    debugInfo += "\n";*/
                }
            }
        }
    }

    /// <summary>
    /// Cuts the virtual matrix to the
    /// minimum size.
    /// </summary>
    /// <returns>A matrix holding game fields in shrinked size.</returns>
    private GameObject[,] CutVirtualMatrix()
    {
        int minCol = virtualFieldMatrix.GetLength(1);
        int maxCol = 0;
        int minRow = virtualFieldMatrix.GetLength(0);
        int maxRow = 0;

        //get boundaries
        for (int i = 0; i < virtualFieldMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < virtualFieldMatrix.GetLength(1); j++)
            {
                if(virtualFieldMatrix[i,j] != null)
                {
                    if (minRow > i)
                        minRow = i;
                    if (maxRow < i)
                        maxRow = i;
                    if (minCol > j)
                        minCol = j;
                    if (maxCol < j)
                        maxCol = j;
                }
                else
                {
                    //fill with random norm tile                    
                    int rndDegreeMultiplier = Random.Range(1, 4);
                    GameObject rndNormTile = normalGameFields[Random.Range(0, normalGameFields.Count)];
                    rndNormTile.transform.rotation = Quaternion.Euler(0, rndDegreeMultiplier * 90, 0);
                    virtualFieldMatrix[i, j] = rndNormTile;
                }
            }
        }

        //create temp matrix with new dimensions
        GameObject[,] tempMatrix = new GameObject[maxRow - minRow + 1, maxCol - minCol + 1];

        //copy elements 
        int rowCounter = 0;
        int colCounter = 0;
        for (int i = minRow; i <= maxRow; i++)
        {
            colCounter = 0;
            for (int j = minCol; j <= maxCol; j++)
            {
                tempMatrix[rowCounter, colCounter++] = virtualFieldMatrix[i, j];                
            }
            rowCounter++;
        }

        //correct walked field coords
        //should probably not be in this method later on
        //but for now it's fine
        for (int i = 0; i < WalkedFieldCoords.Count; i++)
            WalkedFieldCoords[i] = (WalkedFieldCoords[i].Item1 - minRow + 1, WalkedFieldCoords[i].Item2 - minCol + 1);
        
        return tempMatrix;
    }
        
    /// <summary>
    /// Tries to shift level in case it went
    /// out of game area.
    /// </summary>
    private void FixTiles()
    {
        GameObject start = new GameObject();
        GameObject goal = new GameObject();
        (int, int) startCoords = (0, 0); 
        (int, int) goalCoords = (0, 0);

        int dCol = 0;
        int dRow = 0;

        //first check where goal tile is
        //related to the start tile
        //TODO: This needs to be done more 
        //      sophisticately later, since 
        //      levels could be generated more
        //      irregular etc.
        for (int i = 0; i < GeneratedFields.Count; i++)
        {
            if (GeneratedFields[i].name == "Start")
            {
                start = GeneratedFields[i];
                startCoords = PlacedFieldCoords[i];
            }
            if (GeneratedFields[i].name == "Goal")
            {
                goal = GeneratedFields[i];
                goalCoords = PlacedFieldCoords[i];
            }                
        }

        //shortcuts 
        bool isRightOut = goalCoords.Item2 > gameAreaWidth - 1;
        bool isLeftOut = goalCoords.Item2 < 0;
        bool isBottomOut = goalCoords.Item1 > gameAreaHeight - 1;
        bool isTopOut = goalCoords.Item1 < 0;

        //first reset all to norm tiles        
        for (int i = 0; i < PlacedFieldCoords.Count; i++)
            ReplaceTileWithNormTile(PlacedFieldCoords[i].Item1, PlacedFieldCoords[i].Item2);

        //and start tile
        ReplaceTileWithNormTile(startTileRow, startTileCol);

        if (isRightOut)
        {
            debugInfo += "\n";
            debugInfo += "[INFO] TILE IS RIGHT OUT! MOVING STUFF TO THE LEFT.";
                        
            dCol = goalCoords.Item2 - (gameAreaWidth - 1);

            //shift everything to the left
            for (int i = 0; i < GeneratedFields.Count; i++)            
                ReplaceTile(PlacedFieldCoords[i].Item1, PlacedFieldCoords[i].Item2 - dCol, GeneratedFields[i], GeneratedRotations[i]);

            //also walked fields coords
            for (int i = 0; i < WalkedFieldCoords.Count; i++)
                WalkedFieldCoords[i] = (WalkedFieldCoords[i].Item1, WalkedFieldCoords[i].Item2 - dCol);

            //and saved coords
            for (int i = 0; i < SavedCoords.Count; i++)
                SavedCoords[i] = (SavedCoords[i].Item1, SavedCoords[i].Item2 - dCol);


            //finally replace start tile.. 
            if(gameFieldStart != null)            
                ReplaceTile(startTileRow, startTileCol - dCol, gameFieldStart, 0);

            //..and robo
            startTileCol -= dCol;
            StartCol = startTileCol;

            if (robo != null && !IsOutOfIndex(fieldMatrix, startTileRow, startTileCol))
                robo.transform.position = fieldMatrix[startTileRow, startTileCol].transform.position;
        }
        else if(isLeftOut)
        {
            debugInfo += "\n";
            debugInfo += "[INFO] TILE IS LEFT OUT! MOVING STUFF TO THE RIGHT.";

            dCol = Math.Abs(goalCoords.Item2);

            //shift everything to the right
            for (int i = 0; i < GeneratedFields.Count; i++)
                ReplaceTile(PlacedFieldCoords[i].Item1, PlacedFieldCoords[i].Item2 + dCol, GeneratedFields[i], GeneratedRotations[i]);

            for (int i = 0; i < WalkedFieldCoords.Count; i++)
                WalkedFieldCoords[i] = (WalkedFieldCoords[i].Item1, WalkedFieldCoords[i].Item2 + dCol);

            for (int i = 0; i < SavedCoords.Count; i++)
                SavedCoords[i] = (SavedCoords[i].Item1, SavedCoords[i].Item2 + dCol);

            if (gameFieldStart != null)
                ReplaceTile(startTileRow, startTileCol + dCol, gameFieldStart, 0);
            
            startTileCol += dCol;
            StartCol = startTileCol;
            
            if (robo != null && !IsOutOfIndex(fieldMatrix, startTileRow, startTileCol))
                robo.transform.position = fieldMatrix[startTileRow, startTileCol].transform.position;
        }
        else if (isBottomOut)
        {
            debugInfo += "\n";
            debugInfo += "[INFO] TILE IS BOTTOM OUT! MOVING STUFF TO THE TOP.";

            dRow = goalCoords.Item1 - (gameAreaHeight - 1);

            //shift everything to the top
            for (int i = 0; i < GeneratedFields.Count; i++)
                ReplaceTile(PlacedFieldCoords[i].Item1 - dRow, PlacedFieldCoords[i].Item2, GeneratedFields[i], GeneratedRotations[i]);

            for (int i = 0; i < WalkedFieldCoords.Count; i++)
                WalkedFieldCoords[i] = (WalkedFieldCoords[i].Item1 - dRow, WalkedFieldCoords[i].Item2);

            for (int i = 0; i < SavedCoords.Count; i++)
                SavedCoords[i] = (SavedCoords[i].Item1 - dRow, SavedCoords[i].Item2);

            if (gameFieldStart != null)
                ReplaceTile(startTileRow - dRow, startTileCol, gameFieldStart, 0);
                        
            startTileRow -= dRow;
            StartRow = startTileRow;

            if (robo != null && !IsOutOfIndex(fieldMatrix, startTileRow, startTileCol))
                robo.transform.position = fieldMatrix[startTileRow, startTileCol].transform.position;
        }
        else if (isTopOut)
        {
            debugInfo += "\n";
            debugInfo += "[INFO] TILE IS TOP OUT! MOVING STUFF TO THE BOTTOM.";

            dRow = Math.Abs(goalCoords.Item1);

            //shift everything to the bottom
            for (int i = 0; i < GeneratedFields.Count; i++)
                ReplaceTile(PlacedFieldCoords[i].Item1 + dRow, PlacedFieldCoords[i].Item2, GeneratedFields[i], GeneratedRotations[i]);

            for (int i = 0; i < WalkedFieldCoords.Count; i++)
                WalkedFieldCoords[i] = (WalkedFieldCoords[i].Item1 + dRow, WalkedFieldCoords[i].Item2);

            for (int i = 0; i < SavedCoords.Count; i++)
                SavedCoords[i] = (SavedCoords[i].Item1 + dRow, SavedCoords[i].Item2);

            if (gameFieldStart != null)
                ReplaceTile(startTileRow + dRow, startTileCol, gameFieldStart, 0);

            startTileRow += dRow;
            StartRow = startTileRow;

            if (robo != null && !IsOutOfIndex(fieldMatrix, startTileRow, startTileCol))
                robo.transform.position = fieldMatrix[startTileRow, startTileCol].transform.position;
        }
    }

    /// <summary>
    /// In case some duplicates are saved remove those.
    /// Could be done by Distinct but Orientation list
    /// needs to be altered also.
    /// </summary>
    private void CleanSavedMovements()
    {
        List<(int, int)> resultCoords = new List<(int, int)>();
        List<int> resultOrientation = new List<int>();
        for (int i = 0; i < SavedCoords.Count; i++)
        {
            // Assume not duplicate.
            bool duplicate = false;
            for (int z = 0; z < i; z++)
            {
                if ((SavedCoords[z] == SavedCoords[i]) 
                    && SavedOrientations[z] == SavedOrientations[i])
                {
                    // This is a duplicate.
                    duplicate = true;
                    break;
                }
            }
            // If not duplicate, add to result.
            if (!duplicate)
            {
                resultCoords.Add(SavedCoords[i]);
                resultOrientation.Add(SavedOrientations[i]);
            }
        }

        //finally overwrite
        SavedCoords = resultCoords;
        SavedOrientations = resultOrientation;
    }

    /// <summary>
    /// Saves all steps (including orientation) for later.
    /// </summary>
    private void SaveMovement()
    {
        debugInfo += "\n";
        debugInfo += "SAVING MOVEMENT --> TILE ROW: " + actualRow + ", COL: " + actualCol + ", ROTATION: " + actualOrientation;
        SavedCoords.Add((actualRow, actualCol));
        SavedOrientations.Add(actualOrientation);
    }

    /// <summary>
    /// Changes the material for the game fields 
    /// to some with brighter albedo color.
    /// </summary>
    private void HighlightFields()
    {
        Debug.Log("[INFO] Highlighting fields to walk.");

        foreach (var cord in WalkedFieldCoords)
        {
            //row, col
            if (!IsOutOfIndex(fieldMatrix, cord.Item1, cord.Item2))
            {
                fieldMatrix[cord.Item1, cord.Item2].GetComponent<Renderer>().material.color = normalFieldColor;
                //fix to have standard fields highlighted correctly later
                if (fieldMatrix[cord.Item1, cord.Item2].name.Length == 25)
                    fieldMatrix[cord.Item1, cord.Item2].name = fieldMatrix[cord.Item1, cord.Item2].name.Remove(14, 4);
            }
        }
    }

    /// <summary>
    /// Highlights a certain game field at the
    /// given coordination.
    /// </summary>
    /// <param name="coords">The coordination (row/col) for the game field to highlight.</param>
    private void HighlightField((int, int) coords)
    {
        if(!IsOutOfIndex(fieldMatrix, coords.Item1, coords.Item2))
            fieldMatrix[coords.Item1, coords.Item2].GetComponent<Renderer>().material.color = normalFieldColor;
    }

    //--- LEVEL SAVE ---//
    
    /// <summary>
    /// Saves an actual level into <see cref="LevelData"/>.
    /// </summary>
    public void SaveLevel(int actualLevel)
    {
        if (levelData == null || gameController == null)
            return;

        if (actualLevel >= levelData.SavedLevelMatrices.Length)
            return;

        //game fields
        for (int i = 0; i < fieldMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < fieldMatrix.GetLength(1); j++)
            {
                if(FieldByteRepresentation.TryGetValue(fieldMatrix[i,j].name, out byte fieldRepresenation))
                {
                    levelData.SavedLevelMatrices[actualLevel, i, j] = fieldRepresenation;
                    levelData.SavedFieldOrientations[actualLevel, i, j] = (byte)gameController.GetFieldOrientation(fieldMatrix[i, j]);
                }
            }
        }

        List<string> correctCombinations = new List<string>();

        foreach (var combination in Combinations)
        {
            correctCombinations.Add(combination);
        }

        //correct combinations        
        //TODO: FIX INCORRECT COMBINATION SAVINGS!!!
        //levelData.CorrectCombinations.Add(Combinations);
        levelData.CorrectCombinations.Add(correctCombinations);
    }

    //--- MOVEMENT AND ROTATION ---//

    /// <summary>
    /// Moves the robot into the correct spot according to
    /// the changes caused by game field(s).
    /// </summary>
    /// <param name="fieldRotation">If the field is rotated this param gives the orientation.</param>
    private void ProcessMovementField(int fieldRotation, bool conveyor)
    {
        /*debugInfo += "\n";
        debugInfo += "Moving Robo by Field..." + "\n";
        debugInfo += "Num Moves Field " + numMovesField + "\n"; 
        debugInfo += "Actual Row: " + actualRow + ", Actual Col: " + actualCol + "\n";*/

        //If forward field has been rotated the 
        //new row/col is different, while the robo
        //keeps its own orientation still.
        if (conveyor)
        {
            switch (fieldRotation)
            {
                case 0:
                    SaveWalkedFields(false, false, actualRow, actualCol, numMovesField);
                    actualCol += numMovesField;
                    break;
                case 1:
                    SaveWalkedFields(true, false, actualRow, actualCol, numMovesField);
                    actualRow += numMovesField;
                    break;
                case 2:
                    SaveWalkedFields(false, true, actualRow, actualCol, numMovesField);
                    actualCol -= numMovesField;
                    break;
                case 3:
                    SaveWalkedFields(true, true, actualRow, actualCol, numMovesField);
                    actualRow -= numMovesField;
                    break;
                default:
                    break;
            }
        }
        else
        {
            //in case field is a normal type field highlight 
            if (numMovesField == 0)
                WalkedFieldCoords.Add((actualRow, actualCol));

            switch (actualOrientation)
            {
                case 0:
                    SaveWalkedFields(false, false, actualRow, actualCol, numMovesField);
                    actualCol += numMovesField;
                    break;
                case 2:
                    SaveWalkedFields(false, true, actualRow, actualCol, numMovesField);
                    actualCol -= numMovesField;
                    break;
                case 1:
                    SaveWalkedFields(true, false, actualRow, actualCol, numMovesField);
                    actualRow += numMovesField;
                    break;
                default:
                    SaveWalkedFields(true, true, actualRow, actualCol, numMovesField);
                    actualRow -= numMovesField;
                    break;
            }
        }

        debugInfo += "\n";
        debugInfo += "MOVEMENT FIELD" + "\n";
        debugInfo += "New Row: " + actualRow + ", New Col: " + actualCol + "\n";
    }

    /// <summary>
    /// Moves the robo according to the program card.
    /// </summary>
    private void ProcessMovementCard()
    {
        /*debugInfo += "\n";
        debugInfo += "Moving Robo by Card..." + "\n";
        debugInfo += "Num Moves Card " + numMovesCard + "\n";
        debugInfo += "Actual Row: " + actualRow + ", Actual Col: " + actualCol + "\n";
        debugInfo += "Actual Orientation: " + actualOrientation + "\n";*/

        switch (actualOrientation)
        {
            case 0:                
                SaveWalkedFields(false, false, actualRow, actualCol, numMovesCard);
                actualCol += numMovesCard;
                break;
            case 2:                
                SaveWalkedFields(false, true, actualRow, actualCol, numMovesCard);
                actualCol -= numMovesCard;
                break;
            case 1:                
                SaveWalkedFields(true, false, actualRow, actualCol, numMovesCard);
                actualRow += numMovesCard;
                break;
            default:                
                SaveWalkedFields(true, true, actualRow, actualCol, numMovesCard);
                actualRow -= numMovesCard;
                break;
        }

        debugInfo += "\n";
        debugInfo += "MOVEMENT CARD" + "\n";
        debugInfo += "New Row: " + actualRow + ", New Col: " + actualCol + "\n";
    }

    /// <summary>
    /// Returns the orientation for a field to place if
    /// it's a foward field. The field should not face
    /// the direction from which the robo comes to prevent him
    /// being placed back into that direction.
    /// </summary>
    /// <param name="prevRow">The row from which the robo came.</param>
    /// <param name="prevCol">The col from which the robo came.</param>
    /// <returns>The correct orientation of a conveyor (forward) field to be placed.</returns>
    private int GetFieldRotation(int prevRow, int prevCol)
    {
        int fRotation = 0;
        bool isFacingPrevDir = true;

        while (isFacingPrevDir)
        {
            fRotation = Random.Range(0, NUMBER_ORIENTATIONS);
            //shortcuts
            bool wasBelow = prevRow > actualRow;
            bool wasAbove = prevRow < actualRow;
            bool wasRight = prevCol > actualCol;
            bool wasLeft = prevCol < actualCol;

            if (!((wasBelow && fRotation == 1) || (wasAbove && fRotation == 3) ||
                (wasRight && fRotation == 0) || (wasLeft && fRotation == 2)))
                isFacingPrevDir = false;
        }        
        return fRotation;
    }

    /// <summary>
    /// Returns the new orientation (direction) of the robo
    /// according to the actual program card.
    /// </summary>
    /// <param name="partCard">The actual program card given by a string.</param>
    /// <returns>The new orientation of the robo according to the card command.</returns>
    private int GetOrientationCard(string partCard)
    {
        /*debugInfo += "ORIENTATION CARD" + "\n";
        debugInfo += "OLD: " + actualOrientation + "\n";*/

        int newOrientation = actualOrientation;
        switch (partCard)
        {
            case "C_RR":
                newOrientation = GetNewOrientation(1);
                break;
            case "C_RL":
                newOrientation = GetNewOrientation(-1);
                break;
            case "C_UT":
                newOrientation = GetNewOrientation(2);
                break;
            default:
                break;
        }

        /*debugInfo += "NEW: " + newOrientation + "\n";*/

        return newOrientation;
    }

    /// <summary>
    /// Returns the orientation for the robo according to 
    /// which field he's standing on at the moment.
    /// </summary>
    /// <param name="partField">The field where the robo is standing on atm.</param>
    /// <returns>The actual orientation of the robo according to the field he stands on.</returns>
    private int GetOrientationField(string partField)
    {
        int newOrientation = actualOrientation;
        switch (partField)
        {
            case "F_RR":
                newOrientation = GetNewOrientation(1);
                /*debugInfo += "\n";
                debugInfo += "Orientation changed by field RR. Actual: " + actualOrientation + ", NEW: " + newOrientation + "\n";*/
                break;
            case "F_RL":                
                newOrientation = GetNewOrientation(-1);
                /*debugInfo += "\n";
                debugInfo += "Orientation changed by field RL. Actual: " + actualOrientation + ", NEW: " + newOrientation + "\n";*/
                break;
            default:
                break;
        }
        return newOrientation;
    }

    /// <summary>
    /// Get new Orientation when orientation of the robo has changed
    /// by card or field.
    /// </summary>
    /// <param name="orientationChange"></param>
    /// <returns>The new orientation of the robo after the orientation has changed.</returns>
    public int GetNewOrientation(int orientationChange)
    {
        int newOrienation = 0;
        if (orientationChange > 0 || orientationChange < 0)
        {
            int sum = actualOrientation + orientationChange;
            if (sum > NUMBER_ORIENTATIONS - 1)
                newOrienation = sum - NUMBER_ORIENTATIONS;
            else if (sum < 0)
                newOrienation = NUMBER_ORIENTATIONS - 1; //assuming no UTurns leftwards
            else
                newOrienation = sum;
        }        
        return newOrienation;
    }

    /// <summary>
    /// Returns the number of fields to move 
    /// for the robo, according to the field
    /// he's standing on.
    /// </summary>
    /// <param name="partField">The field where the robo stands on at the moment.</param>
    /// <returns>The number of moves of thr robo from the field.</returns>
    public int GetNumberOfMovesField(string partField)
    {
        int numberOfMovesField = partField switch
        {
            "F_F1" => 1,
            "F_F2" => 2,
            _ => 0,
        };
        return numberOfMovesField;
    }

    /// <summary>
    /// Returns the number of field movements of the robo
    /// according to which program card is in use atm.
    /// </summary>
    /// <param name="partCard">The actual program card.</param>
    /// <returns>The number of moves of the robo from the card.</returns>
    public int GetNumberOfMovesCard(string partCard)
    {
        int numberOfMovesCard = partCard switch
        {
            "C_M1" => 1,
            "C_M2" => 2,
            "C_M3" => 3,
            "C_B" => -1,
            _ => 0,
        };
        return numberOfMovesCard;
    }

    //--- GENERAL LEVEL SETUP HANDLING ---//

    /// <summary>
    /// Stores the string shortcuts for every game
    /// field type and their actual GameObject 
    /// representation in a <see cref="Dictionary{TKey, TValue}"/>.
    /// 
    /// It also creates a <see cref="Dictionary{TKey, TValue}"/>
    /// for the predefined matrices.
    /// </summary>
    public void StoreGameFieldTypes()
    {
        GameFields = new Dictionary<string, GameObject>
        {
            //Initialize Dict for game fields
            { "F_RR", gameFieldRotateRight },
            { "F_RL", gameFieldRotateLeft },
            { "F_N", normalGameFields[0] },
            { "F_F1", gameFieldConveyorSimpleStill },
            { "F_F1_A", gameFieldConveyorSimple },
            { "F_F2", gameFieldConveyorDoubleStill },
            { "F_F2_A", gameFieldConveyorDouble },
            { "F_G", gameFieldGoal },
            { "F_S", gameFieldStart }
        };

        PredefinedGameFields = new Dictionary<byte, string>
        {
            //Initialize the Dict for their byte representation
            { rotateFieldRight, "F_RR" },
            { rotateFieldLeft, "F_RL" },
            { standardFieldDark, "F_N" },
            { standardFieldNorm, "F_N_H"},
            { convFieldSimpleStill, "F_F1"},
            { convFieldSimple, "F_F1_A" },
            { convFieldDoubleStill, "F_F2" },
            { convFieldDouble, "F_F2_A" },
            { goalField, "F_G" },
            { startField, "F_S" }
        };

        FieldByteRepresentation = new Dictionary<string, byte>
        {
            {"ConveyorDouble(Clone)", convFieldDouble},
            {"ConveyorDoubleStill(Clone)", convFieldDoubleStill},
            {"ConveyorSimple(Clone)", convFieldSimple},
            {"ConveyorSimpleStill(Clone)", convFieldSimpleStill},
            {"Goal(Clone)", goalField},
            {"Start(Clone)", startField},
            {"StandardFieldA(Clone)", standardFieldNorm},
            {"StandardFieldB(Clone)", standardFieldNorm},
            {"StandardFieldC(Clone)", standardFieldNorm},
            {"StandardFieldD(Clone)", standardFieldNorm},
            {"StandardFieldADark(Clone)", standardFieldDark},
            {"StandardFieldBDark(Clone)", standardFieldDark},
            {"StandardFieldCDark(Clone)", standardFieldDark},
            {"StandardFieldDDark(Clone)", standardFieldDark},
            {"RotateLeft(Clone)", rotateFieldLeft},
            {"RotateRight(Clone)", rotateFieldRight},
        };
}

    /// <summary>
    /// Stores the set probabilities for card- and game field-spawns.
    /// </summary>
    public void StoreProbabilities()
    {
        FieldProbabilities = new List<KeyValuePair<string, float>>();
        FieldProbabilitiesStart = new List<KeyValuePair<string, float>>();
        CardProbabilities = new List<KeyValuePair<string, float>>();
        CardProbabilitiesStart = new List<KeyValuePair<string, float>>();

        //Initialize starting probablities for game fields
        //for the first field/card-combination
        FieldProbabilitiesStart.Add(new KeyValuePair<string, float>("F_F1", 0.5f));
        FieldProbabilitiesStart.Add(new KeyValuePair<string, float>("F_F2", 0.3f));
        FieldProbabilitiesStart.Add(new KeyValuePair<string, float>("F_N", 0.2f));

        CardProbabilitiesStart.Add(new KeyValuePair<string, float>("C_M1", 0.6f));
        CardProbabilitiesStart.Add(new KeyValuePair<string, float>("C_M2", 0.3f));
        CardProbabilitiesStart.Add(new KeyValuePair<string, float>("C_M3", 0.1f));

        //Initialize the spawn probabilities for the rest 
        //according to entred values from editor
        FieldProbabilities.Add(new KeyValuePair<string, float>("F_F1", (simpleConveyorProbability / 100.0f)));        
        FieldProbabilities.Add(new KeyValuePair<string, float>("F_F2", (doubleConveyorProbability / 100.0f)));
        FieldProbabilities.Add(new KeyValuePair<string, float>("F_FN", (normalFieldProbability / 100.0f)));
        FieldProbabilities.Add(new KeyValuePair<string, float>("F_RR", (rotateRightFieldProbability / 100.0f)));
        FieldProbabilities.Add(new KeyValuePair<string, float>("F_RL", (rotateLeftFieldProbability / 100.0f)));

        CardProbabilities.Add(new KeyValuePair<string, float>("C_M1", (cardMove1Probability / 100.0f)));
        CardProbabilities.Add(new KeyValuePair<string, float>("C_M2", (cardMove2Probability / 100.0f)));
        CardProbabilities.Add(new KeyValuePair<string, float>("C_M3", (cardMove3Probability / 100.0f)));
        CardProbabilities.Add(new KeyValuePair<string, float>("C_RR", (cardRRightProbability / 100.0f)));
        CardProbabilities.Add(new KeyValuePair<string, float>("C_RL", (cardRLeftProbability / 100.0f)));
        CardProbabilities.Add(new KeyValuePair<string, float>("C_UT", (cardUTurnProbability / 100.0f)));
        CardProbabilities.Add(new KeyValuePair<string, float>("C_B", (cardBackProbability / 100.0f)));
    }

    private void AssignProgramCards()
    {
        ProgrammingCards = new Dictionary<string, GameObject>
        {
            { "C_M1", CardMove1 },
            { "C_M2", CardMove2 },
            { "C_M3", CardMove3 },
            { "C_B", CardMoveBack },
            { "C_RR", CardRotateRight },
            { "C_RL", CardRotateLeft },
            { "C_UT", CardUTurn }
        };
    }

    //--- MOVEMENT SAVING AND GENERAL LIST HANDLING ---//

    /// <summary>
    /// Saves all fields the robo has to walk along.
    /// </summary>
    /// <param name="row"></param>
    /// <param name="back"></param>
    /// <param name="actualRow"></param>
    /// <param name="actualCol"></param>
    /// <param name="numMoves"></param>
    private void SaveWalkedFields(bool row, bool back, int actualRow, int actualCol, int numMoves)
    {
        if (row)
        {
            if (back)
            {
                for (int i = actualRow; i > actualRow - numMoves; i--)
                    WalkedFieldCoords.Add((i, actualCol));

            }
            else
            {
                for (int i = actualRow; i < actualRow + numMoves; i++)
                    WalkedFieldCoords.Add((i, actualCol));
            }
        }
        else
        {
            if (back)
            {
                for (int i = actualCol; i > actualCol - numMoves; i--)
                    WalkedFieldCoords.Add((actualRow, i));
            }
            else
            {
                for (int i = actualCol; i < actualCol + numMoves; i++)
                    WalkedFieldCoords.Add((actualRow, i));
            }
        }
    }

    //--- TILE AND ROBO PLACEMENT ---//

    /// <summary>
    /// Places the normal field tiles randomly.
    /// </summary>
    private void PlaceNormTiles()
    {
        float areaWidth = gameArea.GetComponent<MeshRenderer>().bounds.size.z;
        float areaHeight = gameArea.GetComponent<MeshRenderer>().bounds.size.x;
        float gameTileWidth = normalGameFields[0].GetComponent<Renderer>().bounds.size.z;
        float gameTileHeight = normalGameFields[0].GetComponent<Renderer>().bounds.size.x;

        //create offset to place game fields in middle of the game area
        //could be calculated bit more precisely later on...
        int offsetX = (int)((maxTilesWidth - gameAreaWidth) / 1.6);
        int offsetY = (int)((maxTilesHeight - gameAreaHeight) / 1.6);

        float startPosX = gameArea.transform.position.x + (areaWidth / 2) - (gameTileWidth * offsetX);
        float startPosZ = gameArea.transform.position.z + (areaHeight / 2) - (gameTileHeight * offsetY);

        for (int i = 0; i < gameAreaWidth; ++i)
        {
            for (int j = 0; j < gameAreaHeight; ++j)
            {
                Vector3 pos = new Vector3(startPosX, gameArea.transform.position.y, startPosZ);
                Vector3 posOffset = new Vector3(i * normalGameFields[0].GetComponent<Renderer>().bounds.size.x * -1, 0.0f,
                                                j * normalGameFields[0].GetComponent<Renderer>().bounds.size.z * -1);
                //save in array (matrix) and instantiate object right away
                int rndDegreeMultiplier = Random.Range(1, 4);
                fieldMatrix[i, j] = Instantiate(normalGameFields[Random.Range(0, normalGameFields.Count)],
                                                pos + posOffset, Quaternion.Euler(0, rndDegreeMultiplier * 90, 0));
                //rotate debug text to be displayed properly again
                fieldMatrix[i, j].GetComponentInChildren<Text>().transform.Rotate(0, 0, rndDegreeMultiplier * 90);
            }
        }
    }

    /// <summary>
    /// Places the start field. Must be set in the
    /// spot in the LevelBuilder component.
    /// </summary>
    private void PlaceStartField()
    {
        if(gameFieldStart != null)
        {
            //hide default tile
            fieldMatrix[startTileRow, startTileCol].SetActive(false);
            //instantiate the start field and save it in matrix
            fieldMatrix[startTileRow, startTileCol] = Instantiate(gameFieldStart, fieldMatrix[startTileRow, startTileCol].transform.position, Quaternion.identity);
        }
    }

    /// <summary>
    /// Places the start field at a given row and column.
    /// </summary>
    /// <param name="row">The row where to place the start tile.</param>
    /// <param name="col">The column where to place the start tile.</param>
    private void PlaceStartField(int row, int col)
    {
        if (gameFieldStart != null)
        {
            //hide default tile
            fieldMatrix[row, col].SetActive(false);
            //instantiate the start field and save it in matrix
            fieldMatrix[row, col] = Instantiate(gameFieldStart, fieldMatrix[row, col].transform.position, Quaternion.identity);
        }
    }

    /// <summary>
    /// Places the robot onto the start field.
    /// </summary>
    private void PlaceRobo()
    {
        //by now just pick the first entry in the List
        if(roboModels.Count >  0)
           robo = Instantiate(roboModels[0], fieldMatrix[startTileRow, startTileCol].transform.position, Quaternion.identity);        
    }

    /// <summary>
    /// Places a programming card into the 
    /// defined placeholder.
    /// </summary>
    /// <param name="cardType">The card type to be placed</param>
    /// <param name="numbCommand">The actual command number.</param>
    /// <param name="debug">If True cards will be in correct order, otherwise randomly placed.</param>
    private void PlaceCard(string cardType, int numbCommand, bool debug)
    {
        GameObject card;
        Vector3 pos;

        if (!ProgrammingCards.TryGetValue(cardType, out card))
            return;

        if (debug)
        {
            //put cards in correct order for debugging
            pos = cardPlaceholders[numbCommand].transform.position;
            cardPlaceholders[numbCommand] = Instantiate(card, pos, Quaternion.Euler(90, 0, 0));
        }
        else
        {
            //prevent endless loop
            if (FilledPlaceholders.Count == cardPlaceholders.Count)
                return;

            bool placed = false;
            while (!placed)
            {
                int rnd = Random.Range(0, cardPlaceholders.Count);
                if (!FilledPlaceholders.Contains(rnd))
                {
                    //pos = cardPlaceholders[rnd].transform.position;
                    pos = CardPlaceHoldersPos[rnd];
                    cardPlaceholders[rnd] = Instantiate(card, pos, Quaternion.Euler(90, 0, 0));
                    FilledPlaceholders.Add(rnd);
                    placed = true;
                }
            }
        }
    }

    //[Obsolete("Old way of placing game fields.", false)]
    /// <summary>
    /// Places a field of a certain type onto the game field area. 
    /// </summary>
    /// <param name="partField">The type of the game field.</param>
    /// <param name="rotation">The rotation of the field.</param>    
    private void PlaceField(string partField, int rotation, bool savedLevel = false)
    {
        /*debugInfo += "\n";
        debugInfo += "Placing field of type: " + partField + " and with rotation: " + rotation 
                  + " at actualCol: " + actualCol + " and actualRow: " + actualRow + "\n";*/

        if (partField == "")
            return;

        GameObject fieldToPlace;

        //try to get correct game field
        if (!GameFields.TryGetValue(partField, out fieldToPlace))
            return;

        if (!IsOutOfGameArea())
        {
            Vector3 pos = fieldMatrix[actualRow, actualCol].transform.position;
            Destroy(fieldMatrix[actualRow, actualCol]);
            fieldMatrix[actualRow, actualCol] = Instantiate(fieldToPlace, pos, Quaternion.Euler(0, (rotation * 90) + 90, 0));
            CreatedGameObjects.Add(fieldMatrix[actualRow, actualCol]);            
        }
        else
        {
            //just restart for now
            //Debug.LogError("[ERROR] Field out of game area! Restarting level...");
            wasOutOfGameArea = true;
            //gameController.ResetField();
        }

        PlacedFieldCoords.Add((actualRow, actualCol));

        //save field in list               
        GeneratedFields.Add(fieldToPlace);
        GeneratedRotations.Add(rotation);
        //GeneratedFields[GeneratedFields.Count - 1].transform.rotation = Quaternion.Euler(0, (rotation * 90) + 90, 0);

        //if field is forward 2x place another one next to it
        if (partField == "F_F2" && !savedLevel)
        {
            int rowScndField = actualRow;
            int colScndField = actualCol;

            switch (rotation)
            {
                case 0:
                    colScndField += 1;
                    break;
                case 1:
                    rowScndField += 1;
                    break;
                case 2:
                    colScndField -= 1;
                    break;
                case 3:
                    rowScndField -= 1;
                    break;
                default:
                    break;
            }

            /*if (!(rowScndField >= fieldMatrix.GetLength(0) || colScndField >= fieldMatrix.GetLength(1)
                || rowScndField < 0 || colScndField < 0))*/

            if(!IsOutOfIndex(fieldMatrix, rowScndField, colScndField))
            {
                Vector3 pos = fieldMatrix[rowScndField, colScndField].transform.position;
                Destroy(fieldMatrix[rowScndField, colScndField]);
                fieldMatrix[rowScndField, colScndField] = Instantiate(fieldToPlace, pos, Quaternion.Euler(0, (rotation * 90) + 90, 0));
            }

            PlacedFieldCoords.Add((rowScndField, colScndField));

            GeneratedFields.Add(fieldToPlace);
            GeneratedRotations.Add(rotation);
            //GeneratedFields[GeneratedFields.Count - 1].transform.rotation = Quaternion.Euler(0, (rotation * 90) + 90, 0);
        }
    }

    /// <summary>
    /// Replaces a tile at a given row and column with
    /// a normal game field (neutral) tile.
    /// </summary>
    /// <param name="row">The row where to replace the tile.</param>
    /// <param name="col">The column where to replace the tile.</param>
    private void ReplaceTileWithNormTile(int row, int col)
    {
        if (row >= fieldMatrix.GetLength(0) || col >= fieldMatrix.GetLength(1)
            || row < 0 || col < 0)
            return;

        Vector3 pos = fieldMatrix[row, col].transform.position;
        Destroy(fieldMatrix[row, col]);
        int rndDegreeMultiplier = Random.Range(1, 4);
        fieldMatrix[row, col] = Instantiate(normalGameFields[Random.Range(0, normalGameFields.Count)],
                                            pos, Quaternion.Euler(0, rndDegreeMultiplier * 90, 0));
    }

    /// <summary>
    /// Replaces a tile with a given replacement tile.
    /// </summary>
    /// <param name="row">The row where to replace the tile.</param>
    /// <param name="col">The column where to replace the tile.</param>
    /// <param name="replacementTile">The tile replacement.</param>
    /// <param name="rotation">The rotation of the replacement tile.</param>
    private void ReplaceTile(int row, int col, GameObject replacementTile, int rotation)
    {
        if (row >= fieldMatrix.GetLength(0) || col >= fieldMatrix.GetLength(1)
            || row < 0 || col < 0)
            return;

        Vector3 pos = fieldMatrix[row, col].transform.position;
        Destroy(fieldMatrix[row, col]);
        fieldMatrix[row, col] = Instantiate(replacementTile, pos, Quaternion.Euler(0, (rotation * 90) + 90, 0));
    }

    //--- HELPERS ---//

    /// <summary>
    /// Checks whether a certain tile has already a 
    /// game field placed onto it or not.
    /// </summary>
    /// <param name="coords">The coords to check for.</param>
    /// <returns>True if a game field has already been placed at the given coords, false if not.</returns>
    private bool IsFieldOccupied((int, int) coords)
    {
        return !fieldMatrix[coords.Item1, coords.Item2].name.Contains("Standard");               
    }

    /// <summary>
    /// Returns the coordinations of the first index
    /// found of the given game field type.
    /// </summary>
    /// <param name="fields">The matrix of field representations to search in.</param>
    /// <param name="fieldToSearch">The field representation to search the index for.</param>
    /// <returns>The coordinations for a certain game field.</returns>
    public (int, int) GetCoordsOfField(byte[,] fields, byte fieldToSearch)
    {
        (int, int) coords = (0, 0);

        for (int i = 0; i < fields.GetLength(0); i++)
        {
            for (int j = 0; j < fields.GetLength(1); j++)
            {
                if(fields[i,j] == fieldToSearch)
                {
                    coords = (i, j);
                    break;
                }
            }
        }

        return coords;
    }

    /// <summary>
    /// Checks if a given row and column of a 
    /// matrix of <see cref="GameObject"/>s are out of bounds.
    /// </summary>
    /// <param name="fieldMatrix">The matrix to check for.</param>
    /// <param name="actualRow">The row to check.</param>
    /// <param name="actualCol">The column to check.</param>
    /// <returns>True if the coordinates are out of bounds, false if not.</returns>
    private bool IsOutOfIndex(GameObject[,] fieldMatrix, int actualRow, int actualCol)
    {
        if ((actualRow >= fieldMatrix.GetLength(0)) ||
              (actualCol >= fieldMatrix.GetLength(1)) ||
              actualRow < 0 || actualCol < 0)
            return true;
        return false;
    }

    /// <summary>
    /// Helper to check if actual row and/or column where
    /// a tile should be placed is already out of the
    /// game area.
    /// </summary>
    /// <param name="actualRow">The row in the game field where a tile should be checked for.</param>
    /// <param name="actualCol">The column in the game field where a tile should be checked for.</param>
    /// <returns>True if the tile is already out of the game area.</returns>
    private bool IsOutOfGameArea()
    {
        if (actualRow >= fieldMatrix.GetLength(0) ||
           actualCol >= fieldMatrix.GetLength(1) ||
           actualRow < 0 || actualCol < 0)
        {
            debugInfo += "\n";
            debugInfo += "FIELD TO PLACE IS OUT OF BOUNDS OF GAME AREA!" + "\n";
            debugInfo += "ACTUAL ROW: " + actualRow + ", ACTUAL COL: " + actualCol + "\n";

            return true;
        }
        return false;
    }

    /// <summary>
    /// Returns the (2-dimensional) matrix at a given index from
    /// one of the (3-dimensional) array of predefined
    /// matrices for the tutorial level creation.
    /// </summary>
    /// <param name="matrices">The array holding the matrices to look for.</param>
    /// <param name="index">The index to look for.</param>
    /// <returns>A matrix (2-dimensional) at the given indes.</returns>
    private byte[,] GetPredefinedMatrix(byte[,,] matrices, byte index)
    {
        byte[,] result = new byte[0, 0];

        if (index < matrices.GetLength(0))
        {
            //redim result matrix
            result = new byte[matrices.GetLength(1), matrices.GetLength(2)];

            //TODO: Matrices could be different in
            //      their dimenstions. Probably an 
            //      implementation via Lists would
            //      be better then.
            for (int i = 0; i < matrices.GetLength(1); i++)
            {
                for (int j = 0; j < matrices.GetLength(2); j++)
                {
                    result[i, j] = matrices[index, i, j];
                }
            }
        }

        return result;
    }

    /// <summary>
    /// A bit similar to <see cref="GetPredefinedMatrix(byte[,,], byte)"/>
    /// but in this case for the predefined card list.
    /// </summary>
    /// <param name="arrays">The arrays to look for.</param>
    /// <param name="index">The index of the array, that should be returned.</param>
    /// <returns>The predefined string array at the given index.</returns>
    private string[] GetPredefinedArray(string[,] arrays, byte index)
    {
        string[] result = new string[0];

        if (index < arrays.GetLength(0))
        {
            //redim result matrix
            result = new string[arrays.GetLength(1)];
                       
            for (int i = 0; i < arrays.GetLength(1); i++)
            {
                result[i] = arrays[index, i];
            }
        }

        return result;
    }

    /// <summary>
    /// Returns a (cumulatively) randomly picked string from a List
    /// of a <see cref="KeyValuePair{string, float}"/>. 
    /// The float values in the KeyValuePair represent
    /// the chance of an string element to be picked.
    /// </summary>
    /// <param name="keyValuePair">The <see cref="KeyValuePair{string, float}"/> to pick the string from.</param>
    /// <returns>A cumulatively randomly picked key string.</returns>
    private string GetRndCumulativeKeyStr(List<KeyValuePair<string, float>> keyValuePair)
    {
        float diceRoll = Random.Range(0.0f, 0.99999f);
        float cumulative = 0.0f;
        string rndKeyStr = "empty";

        for (int i = 0; i < keyValuePair.Count; i++)
        {
            cumulative += keyValuePair[i].Value;
            if (diceRoll < cumulative)
            {
                rndKeyStr = keyValuePair[i].Key;
                break;
            }
        }
        return rndKeyStr;
    }

    /// <summary>
    /// Picks a random picked field or card string from
    /// a <see cref="KeyValuePair{TKey, TValue}"/>. 
    /// The string gets picked according to the pick chances,
    /// defined in the float value of the <see cref="KeyValuePair{TKey, TValue}"/>.
    /// </summary>
    /// <param name="createdCommands">The number of so far created combinations of card- and field-types.</param>
    /// <param name="cardStr">Whether to return a card string or not (field string then).</param>
    /// <returns></returns>
    private string GetRndStr(int createdCommands, bool cardStr)
    {
        string rndStr = "empty";
        if (createdCommands == MAX_COMMANDS - 1 || createdCommands == 0)
        {
            if (cardStr)
                rndStr = GetRndCumulativeKeyStr(CardProbabilitiesStart);
            else
                rndStr = GetRndCumulativeKeyStr(FieldProbabilitiesStart);
        }
        else
        {
            if (cardStr)
                rndStr = GetRndCumulativeKeyStr(CardProbabilities);
            else
                rndStr = GetRndCumulativeKeyStr(FieldProbabilities);
        }

        return rndStr;
    }

    /// <summary>
    /// Checks whether the given dimensions are valid,
    /// according to the game field dimensions.
    /// </summary>
    private void CheckGameFieldDimensions()
    {
        maxTilesWidth = (int)Math.Floor(gameArea.GetComponent<MeshRenderer>().bounds.size.z / normalGameFields[0].GetComponent<Renderer>().bounds.size.z);
        maxTilesHeight = (int)Math.Floor(gameArea.GetComponent<MeshRenderer>().bounds.size.x / normalGameFields[0].GetComponent<Renderer>().bounds.size.x);
        //Debug.Log("Max tiles width: " + maxTilesWidth + ", Max tiles height: " + maxTilesHeight);
        if (gameAreaWidth > maxTilesWidth)
            gameAreaWidth = maxTilesWidth;
        if (gameAreaHeight > maxTilesHeight)
            gameAreaHeight = maxTilesHeight;
        //Set field matrix dimensions
        fieldMatrix = new GameObject[gameAreaWidth, gameAreaHeight];
        //test
        //fieldMatrix = new GameObject[30, 30];
    }

    /// <summary>
    /// Checks whether all types of game fields have been
    /// assigned to this script or not.
    /// </summary>
    /// <returns></returns>
    private bool AllTilesAssigned()
    {
        if (gameArea != null && gameFieldRotateLeft != null
            && gameFieldRotateRight != null && gameFieldConveyorDouble != null
            && gameFieldConveyorSimple != null)
        {
            if (normalGameFields.Count > 0)
            {
                foreach (var obj in normalGameFields)
                {
                    if (obj == null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        Debug.LogError("[ERROR] Some game fields have not been assigned to GameController script.");
        return false;
    }

    /// <summary>
    /// Infos in Debug Window.
    /// </summary>
    private void DebugLists()
    {
        int counter = 1;

        debugInfo += "\n";
        debugInfo += "FINAL COMBINATIONS: " + "\n";

        foreach (var combination in Combinations)
        {
            debugInfo += "Combination " + counter + ": " + combination + "\n";
            counter += 1;
        }

        debugInfo += "\n";
        debugInfo += "PLACED FIELDS:" + "\n";

        foreach (var item in PlacedFieldCoords)
        {
            debugInfo += item + "\n";
        }

        debugInfo += "\n";
        debugInfo += "WALKED FIELDS:" + "\n";

        foreach (var item in WalkedFieldCoords)
        {
            debugInfo += item + "\n";
        }

        debugInfo += "\n";
        debugInfo += "SAVED MOVEMENTS:" + "\n";

        foreach (var item in SavedCoords)
        {
            debugInfo += item.Item1 + ", " + item.Item2 + "\n";
        }

        debugInfo += "\n";
        debugInfo += "SAVED ORIENTATIONS:" + "\n";

        foreach (var item in SavedOrientations)
        {
            debugInfo += item + "\n";
        }
    }

    /// <summary>
    /// Shows debug text.
    /// </summary>
    private void TestShowGridCoords()
    {
        for (int i = 0; i < fieldMatrix.GetLength(0); i++)
            for (int j = 0; j < fieldMatrix.GetLength(1); j++)
                fieldMatrix[i, j].GetComponentInChildren<Text>().text = "row: " + i + ", col: " + j + "\n" + "pos: " + fieldMatrix[i, j].transform.position;
    }
}
