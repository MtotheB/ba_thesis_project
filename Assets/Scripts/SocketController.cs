using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class SocketController : MonoBehaviour
{
    [SerializeField]
    [Tooltip("The GameController component.")]
    private GameController gameController;

    [SerializeField]
    [Tooltip("To store level data.")]
    private LevelData levelData;

    [SerializeField]
    [Tooltip("Handles game time.")]
    private GameTimer gameTimer;

    [SerializeField]
    [Tooltip("The material to be used for the socket for highlighting.")]
    private Material highlightMaterial;

    [SerializeField]
    [Tooltip("The default socket material.")]
    private Material normalMaterial;

    [SerializeField]
    [Tooltip("The material to be used to indicate, that the socket can't be used.")]
    private Material cantPlaceMaterial;

    [SerializeField]
    [Tooltip("Default socket. Doesn't have to be filled.")]
    private GameObject actualSocket;

    [SerializeField]
    [Tooltip("The card slots for the robo programming cards.")]
    private GameObject[] cardSlots;
    
    /// <summary>
    /// Number of card slots that have been filled.
    /// </summary>
    private int filledSlots;

    /// <summary>
    /// Dictionary to save the programming cards within the slots.
    /// </summary>
    private Dictionary<GameObject, GameObject> slotCardsDict;

    /// <summary>
    /// The last time a card was placed.
    /// </summary>
    private float lastTime;

    /// <summary>
    /// Temporary list to save times elapsed
    /// between cards.
    /// </summary>
    private List<float> cardTimes;

    // Start is called before the first frame update
    void Start()
    {
        //init dict
        slotCardsDict = new Dictionary<GameObject, GameObject>();
        PlacedCards = new List<GameObject>();
        //lists
        cardTimes = new List<float>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //-- EXPORT ---//

    /// <summary>
    /// A list storing all programming cards that
    /// have been placed into the card slots.
    /// </summary>
    public List<GameObject> PlacedCards { get; set; }

    public void MyNewFunc()
    {

    }
    
    /// <summary>
    /// Toggles the highlighting for a socket
    /// by switching it's material.
    /// </summary>
    /// <param name="highlight">Whether to highlight the socket or not.</param>
    public void ToggleHighlight(bool highlight)
    {       
        if(actualSocket != null)
        {
            if(highlightMaterial != null && normalMaterial != null)
            {
                if(highlight)
                    actualSocket.GetComponent<Renderer>().material = highlightMaterial;
                else
                    actualSocket.GetComponent<Renderer>().material = normalMaterial;
            }
            else
            {
                Debug.LogError("[ERROR]: Can't highlight actualSocket. Hoover or Normal Material is null!");
            }
        }
        else
        {
            Debug.LogError("[ERROR]: Can't highlight actualSocket. Socket is null!");
        }
    }

    /// <summary>
    /// Assign the used socket to the actual one.
    /// </summary>
    /// <param name="usedSocket">The socket in use.</param>
    public void SetSocket(GameObject usedSocket)
    {
        actualSocket = usedSocket;
    }    

    /// <summary>
    /// Saves the program card, that has been
    /// placed into this socket and activates
    /// the next socket.
    /// </summary>
    /// <param name="usedSocket">The socket used.</param>
    public void SaveCard(GameObject usedSocket)
    {
        XRSocketInteractor socket = usedSocket.GetComponentInChildren<XRSocketInteractor>();

        if (socket == null)
        {
            Debug.LogError("[ERROR] Socket Interactor not found!");
            return;
        }
        
        //get card
        GameObject card = socket.selectTarget.gameObject;

        if (card == null)
        {
            Debug.LogError("[ERROR] Programming Card in slot not found!");
            return;
        }

        //save socket/card combination in dict
        if (slotCardsDict.ContainsKey(usedSocket))
        {
            //TODO: Rework card slots!
            /*Rigidbody rbCard = card.GetComponent<Rigidbody>();
            if (rbCard != null)
            {
                card.transform.position += new Vector3(2, -2, 0);
                rbCard.isKinematic = false;
                rbCard.useGravity = true;
                rbCard.constraints = RigidbodyConstraints.None;
            }
            else
            {
                Debug.LogError("[ERROR] Couldn't find Rigidbody of card: " + card.name);
            }*/
            return;
        }

        //test
        SphereCollider scSocket = usedSocket.GetComponentInChildren<SphereCollider>();
        if(scSocket != null)
        {
            scSocket.enabled = false;
        }
        else
        {
            Debug.LogError("[ERROR] SphereCollider of socket " + usedSocket.name + " couldn't be found");
        }

        //save time that has elapsed
        if (!gameController.TutorialRunning && levelData != null && gameTimer != null)
        {
            Debug.Log("Adding card time...");
            cardTimes.Add(gameTimer.TimeElapsed - lastTime);            
        }

        lastTime = gameTimer.TimeElapsed;

        //and freeze movement/rotation in slot
        //after short delay to prevent card being
        //freezed in weird position
        StartCoroutine(FreezeCardInSlot(card));

        //save socket/card combination in dict
        if (!slotCardsDict.ContainsKey(usedSocket))
        {
            slotCardsDict.Add(usedSocket, card);
            PlacedCards.Add(card);
        }

        filledSlots += 1;

        if (!(filledSlots >= cardSlots.Length))
            cardSlots[filledSlots].SetActive(true);
                
        //finally deactivate mesh renderer of the slot
        ToggleMeshRenderer(usedSocket, false);

        //when all slots have been filled 
        //start moving the robo (show rounds)        
        if (gameController != null)
        {
            if (filledSlots == gameController.MAX_PROGRAM_CARDS)
            {
                //reset all sphere colliders
                foreach (var socketItem in slotCardsDict)
                {
                    SphereCollider scSocketItem = socketItem.Key.GetComponentInChildren<SphereCollider>();
                    if (scSocketItem != null)
                    {
                        scSocketItem.enabled = true;
                    }
                    else
                    {
                        Debug.LogError("[ERROR] SphereCollider of socket " + usedSocket.name + " couldn't be found");
                    }
                }

                gameController.MoveRobo();
                GameObject timeController = GameObject.Find("TimerController");

                if (timeController != null)
                {
                    GameTimer gt = timeController.GetComponent<GameTimer>();
                    gt.PauseTimer();

                    if (gameController != null && !gameController.TutorialRunning)
                    {

                        //save time for actual level
                        levelData.LevelTimes.Add(gameTimer.TimeElapsed);
                        //save card times
                        List<float> copy = new List<float>();
                        for (int i = 0; i < cardTimes.Count; i++)
                        {
                            //Debug.Log("card times[" + i + "]: " + cardTimes[i]);
                            copy.Add(cardTimes[i]);
                        }

                        levelData.TimeBetweenPlacedCards.Add(copy);

                        cardTimes.Clear();
                    }
                    else
                    {
                        Debug.Log("[INFO] Tutorial running. No data saved!");
                    }
                }
            }
        }
        else
        {
            Debug.LogError("[ERROR] GameController component not found or is null!");
        }
    }

    [Obsolete("Not finished yet. To be used later when card slots get reworked!", true)]
    /// <summary>
    /// Fires when a card is removed from a 
    /// card socket.
    /// 
    /// NOT FINISHED YET.
    /// </summary>
    /// <param name="usedSocket"></param>
    public void RemoveCard(GameObject usedSocket)
    {

        XRSocketInteractor socket = usedSocket.GetComponentInChildren<XRSocketInteractor>();

        if (socket == null)
        {
            Debug.LogError("[ERROR] Socket Interactor not found!");
            return;
        }

        //get card
        GameObject card = socket.selectTarget.gameObject;

        if (card == null)
        {
            Debug.LogError("[ERROR] Programming Card in slot not found!");
            return;
        }

        //save socket/card combination in dict
        if (slotCardsDict.ContainsKey(usedSocket))
        {
            slotCardsDict.Remove(usedSocket);
            slotCardsDict.Add(usedSocket, card);
            if (PlacedCards.Contains(card))
                PlacedCards.Remove(card);            
        }

        filledSlots -= 1;

        if (!(filledSlots >= cardSlots.Length))
            cardSlots[filledSlots].SetActive(true);

        //finally deactivate mesh renderer of the slot
        ToggleMeshRenderer(usedSocket, false);

    }

    /// <summary>
    /// Activates or deactivates the <see cref="MeshRenderer"/> for a <see cref="GameObject"/>.
    /// </summary>
    /// <param name="socket">The socket (<see cref="GameObject"/>) for which the <see cref="MeshRenderer"/> shall be activated or deactivated.</param>
    /// <param name="activate">Whether the mesh of a socket should be enabled or disabled.</param>
    private void ToggleMeshRenderer(GameObject socket, bool activate)
    {
        if (socket == null)
        {
            Debug.LogError("SOCKET NULL! Name: " + socket.name);
            return;
        }

        socket.GetComponent<MeshRenderer>().enabled = activate;
    }

    /// <summary>
    /// Freezes a program card in place after it
    /// was placed into a socket. This happens after a short delay
    /// to not freeze the card in a weird position.
    /// </summary>
    /// <param name="card">The programming card to be placed into the slot.</param>
    /// <returns>Delay in seconds.</returns>
    IEnumerator FreezeCardInSlot(GameObject card)
    {
        yield return new WaitForSeconds(0.2f);
        card.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;
        card.GetComponent<XRGrabInteractable>().enabled = false;
    }
    
    /// <summary>
    /// Switches the material of a slot to the 
    /// defined material for something can't be
    /// placed here.
    /// </summary>
    public void HighlightCantPlace()
    {
        if (actualSocket != null)
        {
            if (cantPlaceMaterial != null)
            {
                actualSocket.GetComponent<Renderer>().material = cantPlaceMaterial;
            }
            else
            {
                Debug.LogError("[ERROR]: Can't highlight actualSocket. Material is null!");
            }
        }
        else
        {
            Debug.LogError("[ERROR]: Can't show that object cant be placed at actualSocket. Socket is null!");
        }
    }

    /// <summary>
    /// Resets all card sockets.
    /// </summary>
    public void ResetSockets()
    {
        slotCardsDict.Clear();
        PlacedCards.Clear();

        for (int i = 0; i < cardSlots.Length; i++)
        {
            //enable MeshRenderer again
            ToggleMeshRenderer(cardSlots[i], true);
            //leave first slot open again
            if(i > 0)
                cardSlots[i].SetActive(false);
        }

        filledSlots = 0;
    }

    /// <summary>
    /// Activates or deactivates a certain card slot.
    /// </summary>
    /// <param name="number">The number of the socket to activate or deactivate.</param>
    /// <param name="activate">Whether this slot shall be activated or deactivated.</param>
    public void ToggleSocket(byte number, bool activate)
    {
        if (number >= cardSlots.Length)
            return;

        cardSlots[number].SetActive(activate);
    }
}
