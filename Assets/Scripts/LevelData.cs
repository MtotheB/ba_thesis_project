using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelData : MonoBehaviour

{
    [SerializeField]
    private LevelBuilder levelBuilder;

    [SerializeField]
    private GameController gameController;

    /// <summary>
    /// The saved levels in form of a byte
    /// representation. Check <see cref="LevelBuilder"/> for
    /// info about which number (byte) represents
    /// which field.
    /// </summary>
    public byte[,,] SavedLevelMatrices;

    /// <summary>
    /// The saved orientations of the fields 
    /// of the levels saved in <see cref="savedLevelMatrices"/>.
    /// </summary>
    public byte[,,] SavedFieldOrientations;

    /// <summary>
    /// The correct combinations for each level.
    /// </summary>
    public List<List<string>> CorrectCombinations;

    /// <summary>
    /// The cards the player placed in each level.
    /// </summary>
    public List<List<string>> PlacedCards;

    /// <summary>
    /// The number of correct placed cards each level.
    /// </summary>
    [HideInInspector]
    public List<int> CorrectPlacedCards;

    /// <summary>
    /// The time that has elapsed between
    /// the placement from one slot to 
    /// the next for each level.    
    /// </summary>
    public List<List<float>> TimeBetweenPlacedCards;

    /// <summary>
    /// The time the player needed
    /// for each level.
    /// </summary>
    [HideInInspector]
    public List<float> LevelTimes;

    /// <summary>
    /// The total time elapsed for all levels.
    /// </summary>
    [HideInInspector]
    public float TotalTime;

    /// <summary>
    /// The times when a card was grabbed.
    /// </summary>
    [HideInInspector]
    public List<float> TimesCardGrabbed;

    /// <summary>
    /// The times when a card was dropped.
    /// </summary>
    [HideInInspector]
    public List<float> TimesCardDropped;

    /// <summary>
    /// The cards that were grabbed at specific time.
    /// </summary> 
    [HideInInspector]
    public List<string> CardsGrabbed;

    /// <summary>
    /// The cards that were dropped at a specific time.
    /// </summary>
    [HideInInspector]
    public List<string> CardsDropped;

    /// <summary>
    /// /// The Level number at which a card was grabbed.
    /// </summary>
    [HideInInspector]
    public List<int> CardsGrabbedLevel;

    /// <summary>
    /// The Level number at which a card was dropped.
    /// </summary>
    [HideInInspector]
    public List<int> CardsDroppedLevel;

    // Start is called before the first frame update
    void Start()
    {
        //prepare matrices/initialize lists
        if(levelBuilder != null && gameController != null)
        {
            SavedLevelMatrices = new byte[gameController.numberOfLevels, levelBuilder.FieldDimensions.Item1, levelBuilder.FieldDimensions.Item2];
            SavedFieldOrientations = new byte[gameController.numberOfLevels, levelBuilder.FieldDimensions.Item1, levelBuilder.FieldDimensions.Item2];
            CorrectCombinations = new List<List<string>>();
            PlacedCards = new List<List<string>>();
            CorrectPlacedCards = new List<int>();
            TimeBetweenPlacedCards = new List<List<float>>();
            LevelTimes = new List<float>();
            TimesCardGrabbed = new List<float>();
            TimesCardDropped = new List<float>();
            CardsGrabbed = new List<string>();
            CardsDropped = new List<string>();
            CardsGrabbedLevel = new List<int>();
            CardsDroppedLevel = new List<int>();
        }
        else
        {
            Debug.LogError("[ERROR] LevelBuilder or GameController not assigned.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SaveCardGrabbed(string card)
    {
        GameTimer gt = GameObject.Find("TimerController").GetComponent<GameTimer>();

        if (gt == null || gameController == null)
        {
            Debug.LogError("Game Timer or Game Controller not found!");
            return;
        }

        TimesCardGrabbed.Add(gt.TimeElapsed);
        CardsGrabbed.Add(card);
        CardsGrabbedLevel.Add(gameController.ActualLevelNumber);
    }

    public void SaveCardDropped(string card)
    {
        GameTimer gt = GameObject.Find("TimerController").GetComponent<GameTimer>();

        if (gt == null || gameController == null)
        {
            Debug.LogError("Game Timer or Game Controller not found!");
            return;
        }

        TimesCardDropped.Add(gt.TimeElapsed);
        CardsDropped.Add(card);
        CardsDroppedLevel.Add(gameController.ActualLevelNumber);
    }

}
