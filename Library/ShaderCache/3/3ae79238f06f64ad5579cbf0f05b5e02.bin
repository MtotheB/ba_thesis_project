`G  <Q                         _ADDITIONAL_LIGHTS     _ADDITIONAL_LIGHT_SHADOWS       l?  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 unity_OrthoParams;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_LightmapIndex;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
in highp vec3 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TANGENT0;
out highp vec3 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec4 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD3;
out highp vec3 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
out highp vec4 vs_TEXCOORD7;
vec3 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump vec3 u_xlat16_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_4;
float u_xlat15;
bool u_xlatb15;
void main()
{
    u_xlat0.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = u_xlat1 + hlslcc_mtx4x4unity_MatrixVP[3];
    vs_TEXCOORD0.xyz = u_xlat0.xyz;
    u_xlat0.xyz = (-u_xlat0.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat15 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat15 = max(u_xlat15, 1.17549435e-38);
    u_xlat16_2.x = inversesqrt(u_xlat15);
    u_xlat1.xyz = u_xlat1.xyz * u_xlat16_2.xxx;
    vs_TEXCOORD1.xyz = u_xlat1.xyz;
    u_xlat3.xyz = in_TANGENT0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat3.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_TANGENT0.xxx + u_xlat3.xyz;
    u_xlat3.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_TANGENT0.zzz + u_xlat3.xyz;
    u_xlat15 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat15 = max(u_xlat15, 1.17549435e-38);
    u_xlat16_2.x = inversesqrt(u_xlat15);
    vs_TEXCOORD2.xyz = u_xlat16_2.xxx * u_xlat3.xyz;
    vs_TEXCOORD2.w = in_TANGENT0.w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb15 = !!(unity_OrthoParams.w==0.0);
#else
    u_xlatb15 = unity_OrthoParams.w==0.0;
#endif
    vs_TEXCOORD3.x = (u_xlatb15) ? u_xlat0.x : hlslcc_mtx4x4unity_MatrixV[0].z;
    vs_TEXCOORD3.y = (u_xlatb15) ? u_xlat0.y : hlslcc_mtx4x4unity_MatrixV[1].z;
    vs_TEXCOORD3.z = (u_xlatb15) ? u_xlat0.z : hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat16_2.x = u_xlat1.y * u_xlat1.y;
    u_xlat16_2.x = u_xlat1.x * u_xlat1.x + (-u_xlat16_2.x);
    u_xlat16_0 = u_xlat1.yzzx * u_xlat1.xyzz;
    u_xlat16_4.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_4.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_4.z = dot(unity_SHBb, u_xlat16_0);
    u_xlat16_2.xyz = unity_SHC.xyz * u_xlat16_2.xxx + u_xlat16_4.xyz;
    vs_TEXCOORD5.xyz = u_xlat16_2.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD7 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
vec4 ImmCB_0[4];
uniform 	vec4 _MainLightPosition;
uniform 	mediump vec4 _MainLightColor;
uniform 	mediump vec4 _AdditionalLightsCount;
uniform 	vec4 _AdditionalLightsPosition[16];
uniform 	mediump vec4 _AdditionalLightsColor[16];
uniform 	mediump vec4 _AdditionalLightsAttenuation[16];
uniform 	mediump vec4 _AdditionalLightsSpotDir[16];
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _MainLightShadowParams;
uniform 	vec4 hlslcc_mtx4x4_AdditionalLightsWorldToShadow[64];
uniform 	mediump vec4 _AdditionalShadowParams[16];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_LightmapIndex;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM vec4 _BaseColor;
	UNITY_UNIFORM float _Transparency;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
UNITY_LOCATION(0) uniform mediump samplerCube unity_SpecCube0;
UNITY_LOCATION(1) uniform mediump sampler2D _AdditionalLightsShadowmapTexture;
UNITY_LOCATION(2) uniform mediump sampler2DShadow hlslcc_zcmp_AdditionalLightsShadowmapTexture;
in highp vec3 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD3;
in highp vec3 vs_TEXCOORD5;
layout(location = 0) out mediump vec4 SV_TARGET0;
mediump vec4 u_xlat16_0;
vec3 u_xlat1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
vec4 u_xlat4;
mediump vec4 u_xlat16_4;
mediump vec3 u_xlat16_5;
vec3 u_xlat6;
uint u_xlatu6;
mediump vec3 u_xlat16_7;
vec3 u_xlat8;
vec3 u_xlat9;
vec3 u_xlat10;
mediump vec3 u_xlat16_11;
mediump vec3 u_xlat16_14;
vec3 u_xlat18;
int u_xlati18;
bool u_xlatb18;
float u_xlat30;
uint u_xlatu30;
mediump float u_xlat16_36;
float u_xlat37;
uint u_xlatu37;
mediump float u_xlat16_38;
mediump float u_xlat16_39;
float u_xlat42;
float u_xlat44;
mediump float u_xlat16_44;
int u_xlati44;
bool u_xlatb44;
bool u_xlatb45;
void main()
{
ImmCB_0[0] = vec4(1.0,0.0,0.0,0.0);
ImmCB_0[1] = vec4(0.0,1.0,0.0,0.0);
ImmCB_0[2] = vec4(0.0,0.0,1.0,0.0);
ImmCB_0[3] = vec4(0.0,0.0,0.0,1.0);
    u_xlat16_0.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat16_0.x = inversesqrt(u_xlat16_0.x);
    u_xlat16_0.xyz = u_xlat16_0.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(vs_TEXCOORD3.xyz, vs_TEXCOORD3.xyz);
    u_xlat1.x = max(u_xlat1.x, 1.17549435e-38);
    u_xlat16_2.x = inversesqrt(u_xlat1.x);
    u_xlat1.xyz = u_xlat16_2.xxx * vs_TEXCOORD3.xyz;
    u_xlat16_0.w = 1.0;
    u_xlat16_3.x = dot(unity_SHAr, u_xlat16_0);
    u_xlat16_3.y = dot(unity_SHAg, u_xlat16_0);
    u_xlat16_3.z = dot(unity_SHAb, u_xlat16_0);
    u_xlat16_14.xyz = u_xlat16_3.xyz + vs_TEXCOORD5.xyz;
    u_xlat16_14.xyz = max(u_xlat16_14.xyz, vec3(0.0, 0.0, 0.0));
    SV_TARGET0.w = _Transparency;
#ifdef UNITY_ADRENO_ES3
    SV_TARGET0.w = min(max(SV_TARGET0.w, 0.0), 1.0);
#else
    SV_TARGET0.w = clamp(SV_TARGET0.w, 0.0, 1.0);
#endif
    u_xlat16_3.xyz = _BaseColor.xyz * vec3(0.959999979, 0.959999979, 0.959999979);
    u_xlat16_36 = dot((-u_xlat1.xyz), u_xlat16_0.xyz);
    u_xlat16_36 = u_xlat16_36 + u_xlat16_36;
    u_xlat16_4.xyz = u_xlat16_0.xyz * (-vec3(u_xlat16_36)) + (-u_xlat1.xyz);
    u_xlat16_36 = dot(u_xlat16_0.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_36 = min(max(u_xlat16_36, 0.0), 1.0);
#else
    u_xlat16_36 = clamp(u_xlat16_36, 0.0, 1.0);
#endif
    u_xlat16_36 = (-u_xlat16_36) + 1.0;
    u_xlat16_36 = u_xlat16_36 * u_xlat16_36;
    u_xlat16_36 = u_xlat16_36 * u_xlat16_36;
    u_xlat16_4 = textureLod(unity_SpecCube0, u_xlat16_4.xyz, 4.05000019);
    u_xlat16_39 = u_xlat16_4.w + -1.0;
    u_xlat16_39 = unity_SpecCube0_HDR.w * u_xlat16_39 + 1.0;
    u_xlat16_39 = max(u_xlat16_39, 0.0);
    u_xlat16_39 = log2(u_xlat16_39);
    u_xlat16_39 = u_xlat16_39 * unity_SpecCube0_HDR.y;
    u_xlat16_39 = exp2(u_xlat16_39);
    u_xlat16_39 = u_xlat16_39 * unity_SpecCube0_HDR.x;
    u_xlat16_5.xyz = u_xlat16_4.xyz * vec3(u_xlat16_39);
    u_xlat16_36 = u_xlat16_36 * 0.5 + 0.0399999991;
    u_xlat37 = u_xlat16_36 * 0.941176474;
    u_xlat16_5.xyz = vec3(u_xlat37) * u_xlat16_5.xyz;
    u_xlat16_14.xyz = u_xlat16_14.xyz * u_xlat16_3.xyz + u_xlat16_5.xyz;
    u_xlat16_36 = dot(u_xlat16_0.xyz, _MainLightPosition.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_36 = min(max(u_xlat16_36, 0.0), 1.0);
#else
    u_xlat16_36 = clamp(u_xlat16_36, 0.0, 1.0);
#endif
    u_xlat16_36 = u_xlat16_36 * unity_LightData.z;
    u_xlat16_5.xyz = vec3(u_xlat16_36) * _MainLightColor.xyz;
    u_xlat6.xyz = vs_TEXCOORD3.xyz * u_xlat16_2.xxx + _MainLightPosition.xyz;
    u_xlat37 = dot(u_xlat6.xyz, u_xlat6.xyz);
    u_xlat37 = max(u_xlat37, 1.17549435e-38);
    u_xlat16_36 = inversesqrt(u_xlat37);
    u_xlat6.xyz = vec3(u_xlat16_36) * u_xlat6.xyz;
    u_xlat37 = dot(u_xlat16_0.xyz, u_xlat6.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat37 = min(max(u_xlat37, 0.0), 1.0);
#else
    u_xlat37 = clamp(u_xlat37, 0.0, 1.0);
#endif
    u_xlat6.x = dot(_MainLightPosition.xyz, u_xlat6.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat6.x = min(max(u_xlat6.x, 0.0), 1.0);
#else
    u_xlat6.x = clamp(u_xlat6.x, 0.0, 1.0);
#endif
    u_xlat37 = u_xlat37 * u_xlat37;
    u_xlat37 = u_xlat37 * -0.9375 + 1.00001001;
    u_xlat16_36 = u_xlat6.x * u_xlat6.x;
    u_xlat37 = u_xlat37 * u_xlat37;
    u_xlat6.x = max(u_xlat16_36, 0.100000001);
    u_xlat37 = u_xlat37 * u_xlat6.x;
    u_xlat37 = u_xlat37 * 3.0;
    u_xlat37 = 0.0625 / u_xlat37;
    u_xlat16_36 = u_xlat37 + -6.10351563e-05;
    u_xlat16_7.xyz = vec3(u_xlat16_36) * vec3(0.0399999991, 0.0399999991, 0.0399999991) + u_xlat16_3.xyz;
    u_xlat16_2.xyz = u_xlat16_7.xyz * u_xlat16_5.xyz + u_xlat16_14.xyz;
    u_xlat16_36 = min(_AdditionalLightsCount.x, unity_LightData.y);
    u_xlatu37 =  uint(int(u_xlat16_36));
    u_xlat6.xyz = vs_TEXCOORD0.xyz + (-_WorldSpaceCameraPos.xyz);
    u_xlat6.x = dot(u_xlat6.xyz, u_xlat6.xyz);
    u_xlat6.x = u_xlat6.x * _MainLightShadowParams.z + _MainLightShadowParams.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat6.x = min(max(u_xlat6.x, 0.0), 1.0);
#else
    u_xlat6.x = clamp(u_xlat6.x, 0.0, 1.0);
#endif
    u_xlat16_36 = u_xlat6.x * u_xlat6.x;
    u_xlat16_5.xyz = u_xlat16_2.xyz;
    for(uint u_xlatu_loop_1 = uint(0u) ; u_xlatu_loop_1<u_xlatu37 ; u_xlatu_loop_1++)
    {
        u_xlati18 = int(uint(u_xlatu_loop_1 & 3u));
        u_xlatu30 = uint(u_xlatu_loop_1 >> 2u);
        u_xlat16_38 = dot(unity_LightIndices[int(u_xlatu30)], ImmCB_0[u_xlati18]);
        u_xlati18 = int(u_xlat16_38);
        u_xlat8.xyz = (-vs_TEXCOORD0.xyz) * _AdditionalLightsPosition[u_xlati18].www + _AdditionalLightsPosition[u_xlati18].xyz;
        u_xlat30 = dot(u_xlat8.xyz, u_xlat8.xyz);
        u_xlat30 = max(u_xlat30, 6.10351563e-05);
        u_xlat42 = inversesqrt(u_xlat30);
        u_xlat9.xyz = vec3(u_xlat42) * u_xlat8.xyz;
        u_xlat44 = float(1.0) / float(u_xlat30);
        u_xlat30 = u_xlat30 * _AdditionalLightsAttenuation[u_xlati18].x + _AdditionalLightsAttenuation[u_xlati18].y;
#ifdef UNITY_ADRENO_ES3
        u_xlat30 = min(max(u_xlat30, 0.0), 1.0);
#else
        u_xlat30 = clamp(u_xlat30, 0.0, 1.0);
#endif
        u_xlat30 = u_xlat30 * u_xlat44;
        u_xlat16_38 = dot(_AdditionalLightsSpotDir[u_xlati18].xyz, u_xlat9.xyz);
        u_xlat16_38 = u_xlat16_38 * _AdditionalLightsAttenuation[u_xlati18].z + _AdditionalLightsAttenuation[u_xlati18].w;
#ifdef UNITY_ADRENO_ES3
        u_xlat16_38 = min(max(u_xlat16_38, 0.0), 1.0);
#else
        u_xlat16_38 = clamp(u_xlat16_38, 0.0, 1.0);
#endif
        u_xlat16_38 = u_xlat16_38 * u_xlat16_38;
        u_xlat30 = u_xlat16_38 * u_xlat30;
        u_xlati44 = int(u_xlati18 << 2);
        u_xlat4 = vs_TEXCOORD0.yyyy * hlslcc_mtx4x4_AdditionalLightsWorldToShadow[(u_xlati44 + 1)];
        u_xlat4 = hlslcc_mtx4x4_AdditionalLightsWorldToShadow[u_xlati44] * vs_TEXCOORD0.xxxx + u_xlat4;
        u_xlat4 = hlslcc_mtx4x4_AdditionalLightsWorldToShadow[(u_xlati44 + 2)] * vs_TEXCOORD0.zzzz + u_xlat4;
        u_xlat4 = u_xlat4 + hlslcc_mtx4x4_AdditionalLightsWorldToShadow[(u_xlati44 + 3)];
        u_xlat10.xyz = u_xlat4.xyz / u_xlat4.www;
        vec3 txVec0 = vec3(u_xlat10.xy,u_xlat10.z);
        u_xlat16_44 = textureLod(hlslcc_zcmp_AdditionalLightsShadowmapTexture, txVec0, 0.0);
        u_xlat16_38 = 1.0 + (-_AdditionalShadowParams[u_xlati18].x);
        u_xlat16_38 = u_xlat16_44 * _AdditionalShadowParams[u_xlati18].x + u_xlat16_38;
#ifdef UNITY_ADRENO_ES3
        u_xlatb44 = !!(0.0>=u_xlat10.z);
#else
        u_xlatb44 = 0.0>=u_xlat10.z;
#endif
#ifdef UNITY_ADRENO_ES3
        u_xlatb45 = !!(u_xlat10.z>=1.0);
#else
        u_xlatb45 = u_xlat10.z>=1.0;
#endif
        u_xlatb44 = u_xlatb44 || u_xlatb45;
        u_xlat16_38 = (u_xlatb44) ? 1.0 : u_xlat16_38;
        u_xlat16_39 = (-u_xlat16_38) + 1.0;
        u_xlat16_38 = u_xlat16_36 * u_xlat16_39 + u_xlat16_38;
        u_xlat16_38 = u_xlat16_38 * u_xlat30;
        u_xlat16_39 = dot(u_xlat16_0.xyz, u_xlat9.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlat16_39 = min(max(u_xlat16_39, 0.0), 1.0);
#else
        u_xlat16_39 = clamp(u_xlat16_39, 0.0, 1.0);
#endif
        u_xlat16_38 = u_xlat16_38 * u_xlat16_39;
        u_xlat16_7.xyz = vec3(u_xlat16_38) * _AdditionalLightsColor[u_xlati18].xyz;
        u_xlat18.xyz = u_xlat8.xyz * vec3(u_xlat42) + u_xlat1.xyz;
        u_xlat8.x = dot(u_xlat18.xyz, u_xlat18.xyz);
        u_xlat8.x = max(u_xlat8.x, 1.17549435e-38);
        u_xlat16_38 = inversesqrt(u_xlat8.x);
        u_xlat18.xyz = vec3(u_xlat16_38) * u_xlat18.xyz;
        u_xlat8.x = dot(u_xlat16_0.xyz, u_xlat18.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlat8.x = min(max(u_xlat8.x, 0.0), 1.0);
#else
        u_xlat8.x = clamp(u_xlat8.x, 0.0, 1.0);
#endif
        u_xlat18.x = dot(u_xlat9.xyz, u_xlat18.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlat18.x = min(max(u_xlat18.x, 0.0), 1.0);
#else
        u_xlat18.x = clamp(u_xlat18.x, 0.0, 1.0);
#endif
        u_xlat30 = u_xlat8.x * u_xlat8.x;
        u_xlat30 = u_xlat30 * -0.9375 + 1.00001001;
        u_xlat16_38 = u_xlat18.x * u_xlat18.x;
        u_xlat18.x = u_xlat30 * u_xlat30;
        u_xlat30 = max(u_xlat16_38, 0.100000001);
        u_xlat18.x = u_xlat30 * u_xlat18.x;
        u_xlat18.x = u_xlat18.x * 3.0;
        u_xlat18.x = 0.0625 / u_xlat18.x;
        u_xlat16_38 = u_xlat18.x + -6.10351563e-05;
        u_xlat16_11.xyz = vec3(u_xlat16_38) * vec3(0.0399999991, 0.0399999991, 0.0399999991) + u_xlat16_3.xyz;
        u_xlat16_5.xyz = u_xlat16_11.xyz * u_xlat16_7.xyz + u_xlat16_5.xyz;
    }
    SV_TARGET0.xyz = u_xlat16_5.xyz;
    return;
}

#endif
                             $GlobalsP        _MainLightPosition                           _MainLightColor                         _AdditionalLightsCount                           _AdditionalLightsPosition                    0      _AdditionalLightsColor                   0     _AdditionalLightsAttenuation                 0     _AdditionalLightsSpotDir                 0     _WorldSpaceCameraPos                  0     _MainLightShadowParams                    @     _AdditionalLightsWorldToShadow                   P     _AdditionalShadowParams                  P         UnityPerDraw0        unity_ObjectToWorld                          unity_WorldToObject                         unity_LODFade                            unity_WorldTransformParams                    0      unity_LightData                   @      unity_LightIndices                   P      unity_ProbesOcclusion                     p      unity_SpecCube0_HDR                   �      unity_LightmapST                  �      unity_LightmapIndex                   �      unity_DynamicLightmapST                   �   
   unity_SHAr                    �   
   unity_SHAg                    �   
   unity_SHAb                    �   
   unity_SHBr                    �   
   unity_SHBg                       
   unity_SHBb                      	   unity_SHC                               UnityPerMaterial      
   _BaseColor                           _Transparency                               $Globals@         _WorldSpaceCameraPos                         unity_OrthoParams                           unity_MatrixV                            unity_MatrixVP                    0             unity_SpecCube0                !   _AdditionalLightsShadowmapTexture                   UnityPerDraw              UnityPerMaterial              