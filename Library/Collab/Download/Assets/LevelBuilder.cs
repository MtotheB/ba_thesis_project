using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

/// <summary>
/// Handles the level creation.
/// </summary>
public class LevelBuilder : MonoBehaviour
{
    [Header("Game Objects")]
    [SerializeField]
    [Tooltip("The GameController script for this game.")]
    GameController gameController;
    [SerializeField]
    [Tooltip("The game area on the table.")]
    private GameObject gameArea;
    [SerializeField]
    [Tooltip("Game field Rotate Right.")]
    private GameObject gameFieldRotateRight;
    [SerializeField]
    [Tooltip("Game field Rotate Left.")]
    private GameObject gameFieldRotateLeft;
    [SerializeField]
    [Tooltip("Game field Conveyor Forward.")]
    private GameObject gameFieldConveyorSimple;
    [SerializeField]
    [Tooltip("Game field Conveyor Double Forward.")]
    private GameObject gameFieldConveyorDouble;
    [SerializeField]
    [Tooltip("Game field Goal.")]
    private GameObject gameFieldGoal;
    [SerializeField]
    [Tooltip("Game field Start.")]
    private GameObject gameFieldStart;
    [SerializeField]
    [Tooltip("Normal game fields.")]
    private List<GameObject> normalGameFields;

    [Header("Game Area Definitions")]
    [SerializeField]
    [Tooltip("The number of game field tiles in horizontal direction.")]
    private int gameAreaWidth = 12;
    [SerializeField]
    [Tooltip("The number of game field tiles in vertical direction.")]
    private int gameAreaHeight = 12;
    [SerializeField]
    [Tooltip("The row of the start tile where the robot will be placed initially. Note: The smallest row starts at 0!")]
    private int startTileRow = 6;
    [SerializeField]
    [Tooltip("The column of the start tile where the robot will be placed initially. Note: The smallest column starts at 0!")]
    private int startTileCol = 6;

    [Header("Probabilities Field Types (should add up to 100)")]
    [SerializeField]
    [Range(0, 100)]
    private int rotateRightFieldProbability;
    [SerializeField]
    [Range(0, 100)]
    private int rotateLeftFieldProbability;
    [SerializeField]
    [Range(0, 100)]
    private int simpleConveyorProbability;
    [SerializeField]
    [Range(0, 100)]
    private int doubleConveyorProbability;
    [SerializeField]
    [Range(0, 100)]
    private int normalFieldProbability;

    [Header("Sum Field Probabilities")]
    [SerializeField]
    private int sumProbsFields;

    [Header("Probabilities Card Types (should add up to 100)")]
    [SerializeField]
    [Range(0, 100)]
    private int cardMove1Probability;
    [SerializeField]
    [Range(0, 100)]
    private int cardMove2Probability;
    [SerializeField]
    [Range(0, 100)]
    private int cardMove3Probability;
    [SerializeField]
    [Range(0, 100)]
    private int cardUTurnProbability;
    [SerializeField]
    [Range(0, 100)]
    private int cardRRightProbability;
    [SerializeField]
    [Range(0, 100)]
    private int cardRLeftProbability;
    [SerializeField]
    [Range(0, 100)]
    private int cardBackProbability;

    [Header("Sum Card Probabilities")]
    [SerializeField]
    private int sumProbsCards;

    ///--- EDITOR STUFF ---///
    private int rRFieldProbLast = 0;
    private int rLFieldProbLast = 0;
    private int simpleConvFieldProbLast = 0;
    private int doubleConvFieldProbLast = 0;
    private int normalFieldProbLast = 0;
    private int cardM1ProbLast = 0;
    private int cardM2ProbLast = 0;
    private int cardM3ProbLast = 0;
    private int cardUTurnProbLast = 0;
    private int cardRRProbLast = 0;
    private int cardRLProbLast = 0;
    private int cardBackProbLast = 0;


    /// <summary>
    /// Maximmum possible number of tiles in width.
    /// </summary>
    private int maxTilesWidth;

    /// <summary>
    /// Maximum possible number of tiles in height.
    /// </summary>
    private int maxTilesHeight;

    /// <summary>
    /// This array holds all the placed game field (objects).
    /// </summary>
    private GameObject[,] fieldMatrix;

    //---INTERNAL DEFINES---//

    //Start orientation
    //0 -> looks right (3 o clock)
    //1 -> looks down (6 o clock)
    //2 -> looks left (9 o clock)
    //3 -> looks up (12 o clock)
    private static int START_ORIENTATION = 0;
    private static int NUMBER_ORIENTATIONS = 4;

    //Number of commands to fulfill level
    private static int NUM_COMMANDS = 5;

    //card types
    private static string[] CardTypes =
    {
            "C_M1", "C_M2", "C_M3", "C_B",
            "C_RL", "C_RR", "C_UT"
    };

    //field types
    private static string[] FieldTypes =
    {
            "F_RR", "F_RL", "F_N", "F_F1",
            "F_F2"
    };

    /// <summary>
    /// Holds references for the game fields for
    /// the level creation.
    /// </summary>
    private Dictionary<string, GameObject> GameFields;

    /// <summary>
    /// Here the spawn probablities for certain fame fields are set.
    /// </summary>
    private List<KeyValuePair<string, double>> FieldProbabilities;

    /// <summary>
    /// Here the spawn probablities for certain card types are set.
    /// </summary>
    private List<KeyValuePair<string, double>> CardProbabilities;

    /// <summary>
    /// Here the spawn probablities for the field types 
    /// for the starting combination are set.
    /// </summary>
    private List<KeyValuePair<string, double>> FieldProbabilitiesStart;

    /// <summary>
    /// Here the spawn probablities for the card types 
    /// for the starting combination are set.
    /// </summary>
    private List<KeyValuePair<string, double>> CardProbabilitiesStart;

    /// <summary>
    /// Lists all the invalid field type <> card type combinations.
    /// </summary>
    private static List<string> WrongCombinations = new List<string>()
    {
        {"C_B-F_RR.C_M1-F_F1"},
        {"C_B-F_RR.C_M1-F_F2"},
        {"C_B-F_RR.C_M2-F_F1"},
        {"C_B-F_RR.C_M2-F_F2"},
        {"C_B-F_RR.C_M3-F_F1"},
        {"C_B-F_RR.C_M3-F_F2"},
        {"C_B-F_RR.C_B-F_F1"},
        {"C_B-F_RR.C_B-F_F2"},
        {"C_B-F_RL.C_M1-F_F1"},
        {"C_B-F_RL.C_M1-F_F2"},
        {"C_B-F_RL.C_M2-F_F1"},
        {"C_B-F_RL.C_M2-F_F2"},
        {"C_B-F_RL.C_M3-F_F1"},
        {"C_B-F_RL.C_M3-F_F2"},
        {"C_B-F_RL.C_B-F_F1"},
        {"C_B-F_RL.C_B-F_F2"},
        {"C_B-F_N.C_M1-F_F1"},
        {"C_B-F_N.C_M1-F_F2"},
        {"C_B-F_N.C_M2-F_F1"},
        {"C_B-F_N.C_M2-F_F2"},
        {"C_B-F_N.C_M3-F_F1"},
        {"C_B-F_N.C_M3-F_F2"},
        {"C_B-F_N.C_B-F_F1"},
        {"C_B-F_N.C_B-F_F2"},
        {"C_B-F_F1.C_M1-F_F1"},
        {"C_B-F_F1.C_M1-F_F2"},
        {"C_B-F_F1.C_M2-F_F1"},
        {"C_B-F_F1.C_M2-F_F2"},
        {"C_B-F_F1.C_M3-F_F1"},
        {"C_B-F_F1.C_M3-F_F2"},
        {"C_B-F_F1.C_B-F_F1"},
        {"C_B-F_F1.C_B-F_F2"},
        {"C_B-F_F2.C_M1-F_F1"},
        {"C_B-F_F2.C_M1-F_F2"},
        {"C_B-F_F2.C_M2-F_F1"},
        {"C_B-F_F2.C_M2-F_F2"},
        {"C_B-F_F2.C_M3-F_F1"},
        {"C_B-F_F2.C_M3-F_F2"},
        {"C_B-F_F2.C_B-F_F1"},
        {"C_B-F_F2.C_B-F_F2"}
    };

    /// <summary>
    /// The actual orientation of the robot.
    /// </summary>
    private int actualOrientation;

    /// <summary>
    /// The actual row, where the robot is.
    /// </summary>
    private int actualRow;

    /// <summary>
    /// The actual column, where the robot is.
    /// </summary>
    private int actualCol;

    /// <summary>
    /// Number of movements by the program cards.
    /// </summary>
    private int numMovesCard;

    /// <summary>
    /// Number of movements by the game fields.
    /// </summary>
    private int numMovesField;

    /// <summary>
    /// The changed orientation by the program cads.
    /// </summary>
    private int orientationChangeCard;

    /// <summary>
    /// The changed orientation by the game fields.
    /// </summary>
    private int orientationChangeField;


    void Start()
    {
        //save initial values of field probs 
        SaveProbValues();
    }

    void Update()
    {

    }

    /// <summary>
    /// Gets called when some value got changed.
    /// Used to check if sliders for field/card-spawn
    /// probablities are changed.
    /// </summary>
    private void OnValidate()
    {
        ///--- EDITOR STUFF ---///
        sumProbsFields = simpleConveyorProbability + doubleConveyorProbability +
                         normalFieldProbability + rotateRightFieldProbability +
                         rotateLeftFieldProbability;

        //check which field prob was changed if sum is >100
        if (sumProbsFields > 100)
        {
            //reset slider (value) back so it 
            //doesn't exceed 100 in sum
            if (rRFieldProbLast != rotateLeftFieldProbability)
                rotateRightFieldProbability = rRFieldProbLast;
            if (rLFieldProbLast != rotateLeftFieldProbability)
                rotateLeftFieldProbability = rLFieldProbLast;
            if (simpleConvFieldProbLast != simpleConveyorProbability)
                simpleConveyorProbability = simpleConvFieldProbLast;
            if (doubleConvFieldProbLast != doubleConveyorProbability)
                doubleConveyorProbability = doubleConvFieldProbLast;
            if (normalFieldProbLast != normalFieldProbability)
                normalFieldProbability = normalFieldProbLast;

            sumProbsFields = 100;
        }

        sumProbsCards = cardMove1Probability + cardMove2Probability +
                        cardMove3Probability + cardRRightProbability +
                        cardRLeftProbability + cardUTurnProbability +
                        cardBackProbability;

        if (sumProbsCards > 100)
        {
            if (cardM1ProbLast != cardMove1Probability)
                cardMove1Probability = cardM1ProbLast;
            if (cardM2ProbLast != cardMove2Probability)
                cardMove2Probability = cardM2ProbLast;
            if (cardM3ProbLast != cardMove3Probability)
                cardMove3Probability = cardM3ProbLast;
            if (cardRLProbLast != cardRLeftProbability)
                cardRLeftProbability = cardRLProbLast;
            if (cardRRProbLast != cardRRightProbability)
                cardRRightProbability = cardRRProbLast;
            if (cardUTurnProbLast != cardUTurnProbability)
                cardUTurnProbability = cardUTurnProbLast;
            if (cardBackProbLast != cardBackProbability)
                cardBackProbability = cardBackProbLast;

            sumProbsCards = 100;
        }

        //save last value
        SaveProbValues();
    }

    /// <summary>
    /// Saves the actual values for field- and card-
    /// probabilities for later comparison.
    /// </summary>
    private void SaveProbValues()
    {
        //fields
        rRFieldProbLast = rotateRightFieldProbability;
        rLFieldProbLast = rotateLeftFieldProbability;
        simpleConvFieldProbLast = simpleConveyorProbability;
        doubleConvFieldProbLast = doubleConveyorProbability;
        normalFieldProbLast = normalFieldProbability;
        //cards
        cardM1ProbLast = cardMove1Probability;
        cardM2ProbLast = cardMove2Probability;
        cardM3ProbLast = cardMove3Probability;
        cardRRProbLast = cardRRightProbability;
        cardRLProbLast = cardRLeftProbability;
        cardUTurnProbLast = cardUTurnProbability;
        cardBackProbLast = cardBackProbability;
    }

    /// <summary>
    /// Prepares the initial game field.
    /// </summary>
    public void PrepareGameField()
    {
        //Check for missing tiles in editor.
        if (!AllTilesAssigned())
            return;
        //Check entered dimensions for tiles in horizontal 
        //and vertical direction is too big for game field.
        CheckGameFieldDimensions();
        //prepare game field
        PlaceNormTiles();
        //test
        if (gameController.debug)
            TestShowGridCoords();
    }

    /// <summary>
    /// Checks whether the given dimensions are valid,
    /// according to the game field dimensions.
    /// </summary>
    private void CheckGameFieldDimensions()
    {
        maxTilesWidth = (int)Math.Floor(gameArea.GetComponent<MeshRenderer>().bounds.size.z / normalGameFields[0].GetComponent<Renderer>().bounds.size.z);
        maxTilesHeight = (int)Math.Floor(gameArea.GetComponent<MeshRenderer>().bounds.size.x / normalGameFields[0].GetComponent<Renderer>().bounds.size.x);
        //Debug.Log("Max tiles width: " + maxTilesWidth + ", Max tiles height: " + maxTilesHeight);
        if (gameAreaWidth > maxTilesWidth)
            gameAreaWidth = maxTilesWidth;
        if (gameAreaHeight > maxTilesHeight)
            gameAreaHeight = maxTilesHeight;
        //Set field matrix dimensions
        fieldMatrix = new GameObject[gameAreaWidth, gameAreaHeight];
    }

    /// <summary>
    /// Places the normal field tiles randomly.
    /// </summary>
    private void PlaceNormTiles()
    {
        float areaWidth = gameArea.GetComponent<MeshRenderer>().bounds.size.z;
        float areaHeight = gameArea.GetComponent<MeshRenderer>().bounds.size.x;
        float gameTileWidth = normalGameFields[0].GetComponent<Renderer>().bounds.size.z;
        float gameTileHeight = normalGameFields[0].GetComponent<Renderer>().bounds.size.x;

        //create offset to place game fields in middle of the game area
        //could be calculated bit more precisely later on...
        int offsetX = (int)((maxTilesWidth - gameAreaWidth) / 1.6);
        int offsetY = (int)((maxTilesHeight - gameAreaHeight) / 1.6);

        float startPosX = gameArea.transform.position.x + (areaWidth / 2) - (gameTileWidth * offsetX);
        float startPosZ = gameArea.transform.position.z + (areaHeight / 2) - (gameTileHeight * offsetY);

        for (int i = 0; i < gameAreaWidth; ++i)
        {
            for (int j = 0; j < gameAreaHeight; ++j)
            {
                Vector3 pos = new Vector3(startPosX, gameArea.transform.position.y, startPosZ);
                Vector3 posOffset = new Vector3(i * normalGameFields[0].GetComponent<Renderer>().bounds.size.x * -1, 0.0f, j * normalGameFields[0].GetComponent<Renderer>().bounds.size.z * -1);
                //save in array (matrix) and instantiate object right away
                int rndDegreeMultiplier = Random.Range(1, 4);
                fieldMatrix[i, j] = Instantiate(normalGameFields[Random.Range(0, normalGameFields.Count)], pos + posOffset, Quaternion.Euler(0, rndDegreeMultiplier * 90, 0));
                //rotate debug text to be displayed properly again
                fieldMatrix[i, j].GetComponentInChildren<Text>().transform.Rotate(0, 0, rndDegreeMultiplier * 90);
            }
        }
    }

    /// <summary>
    /// Shows debug text.
    /// </summary>
    private void TestShowGridCoords()
    {
        for (int i = 0; i < fieldMatrix.GetLength(0); i++)
            for (int j = 0; j < fieldMatrix.GetLength(1); j++)
                fieldMatrix[i, j].GetComponentInChildren<Text>().text = "x: " + j + ", y: " + i;
    }

    /// <summary>
    /// Checks whether all types of game fields have been
    /// assigned to this script or not.
    /// </summary>
    /// <returns></returns>
    private bool AllTilesAssigned()
    {
        if (gameArea != null && gameFieldRotateLeft != null
            && gameFieldRotateRight != null && gameFieldConveyorDouble != null
            && gameFieldConveyorSimple != null)
        {
            if (normalGameFields.Count > 0)
            {
                foreach (var obj in normalGameFields)
                {
                    if (obj == null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        Debug.LogError("Some game fields have not been assigned to GameController script.");
        return false;
    }

    /// <summary>
    /// Creates the actual level (problem) to be solved.
    /// </summary>
    public void CreateLevel()
    {
        int numCreatedCommands = 0;
        int prevRow = 0;
        int prevCol = 0;
        string prevCommand = "";

        //assign start orientation
        actualOrientation = START_ORIENTATION;

        //assign start row/col
        actualRow = startTileRow;
        actualCol = startTileCol;

        //place the start field
        PlaceStartField();
    }

    public void StoreGameFieldTypes()
    {
        //Initialize Dict for game fields          
        GameFields.Add("F_RR", gameFieldRotateRight);
        GameFields.Add("F_RL", gameFieldRotateLeft);
        GameFields.Add("F_N", normalGameFields[0]);
        GameFields.Add("F_F1", gameFieldConveyorSimple);
        GameFields.Add("F_G", gameFieldGoal);
        GameFields.Add("F_S", gameFieldStart);
    }

    public void StoreProbabilities()
    {
        //Initialize probablities
        FieldProbabilities.Add(new KeyValuePair<string, double>("F_F1", (simpleConveyorProbability / 100)));
        FieldProbabilities.Add(new KeyValuePair<string, double>("F_F2", (doubleConveyorProbability / 100)));
        FieldProbabilities.Add(new KeyValuePair<string, double>("F_FN", (normalFieldProbability / 100)));
        FieldProbabilities.Add(new KeyValuePair<string, double>("F_RR", (rotateRightFieldProbability / 100)));
        FieldProbabilities.Add(new KeyValuePair<string, double>("F_RL", (rotateLeftFieldProbability / 100)));

        FieldProbabilitiesStart.Add(new KeyValuePair<string, double>("F_F1", 0.5d));
        FieldProbabilitiesStart.Add(new KeyValuePair<string, double>("F_F2", 0.3d));
        FieldProbabilitiesStart.Add(new KeyValuePair<string, double>("F_N", 0.2d));

        CardProbabilities.Add(new KeyValuePair<string, double>("C_M1", (cardMove1Probability / 100)));
        CardProbabilities.Add(new KeyValuePair<string, double>("C_M2", (cardMove2Probability / 100)));
        CardProbabilities.Add(new KeyValuePair<string, double>("C_M3", (cardMove3Probability / 100)));
        CardProbabilities.Add(new KeyValuePair<string, double>("C_RR", (cardRRightProbability / 100)));
        CardProbabilities.Add(new KeyValuePair<string, double>("C_RL", (cardRLeftProbability / 100)));
        CardProbabilities.Add(new KeyValuePair<string, double>("C_UT", (cardUTurnProbability / 100)));
        CardProbabilities.Add(new KeyValuePair<string, double>("C_B", (cardBackProbability / 100)));

        CardProbabilitiesStart.Add(new KeyValuePair<string, double>("C_M1", 0.6d));
        CardProbabilitiesStart.Add(new KeyValuePair<string, double>("C_M2", 0.3d));
        CardProbabilitiesStart.Add(new KeyValuePair<string, double>("C_M3", 0.1d));
    }
  
    /// <summary>
    /// Places the start field. Must be set in the
    /// spot in the LevelBuilder component.
    /// </summary>
    private void PlaceStartField()
    {
        if(gameFieldStart != null)
        {
            Debug.Log("Placing start field...");
            fieldMatrix[startTileCol, startTileRow].SetActive(false);
            //instantiate the start field and save it in matrix
            fieldMatrix[startTileCol, startTileRow] = Instantiate(gameFieldStart, fieldMatrix[startTileCol, startTileCol].transform.position, Quaternion.identity);
        }

    }
}
