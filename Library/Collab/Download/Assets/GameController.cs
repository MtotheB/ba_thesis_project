using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

/// <summary>
/// General game controller script.
/// </summary>
public class GameController : MonoBehaviour
{   
    [Tooltip("Show debug text or not.")]
    public bool debug;

    [SerializeField]
    [Tooltip("The LevelBuilder Component for this game.")]
    LevelBuilder levelBuilder;     
        
    void Start()
    {
        //prepare the default game field
        Debug.Log("Preparing the game field.");
        levelBuilder.PrepareGameField();
        //levelBuilder.StoreGameFieldTypes();
        //levelBuilder.StoreProbabilities();
        levelBuilder.CreateLevel();
    }
    
    void Update()
    {
        
    }
}
